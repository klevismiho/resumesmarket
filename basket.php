<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">Basket</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>BASKET</h1>
                </div>
                <div class="page-title-icon"><img src="images/basket-title.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content basket-content">
    <div class="row">
        <div class="columns">
            <table class="basket-table">
                <thead>
                    <tr>
                        <th colspan="2">NAME AND DESCRIPTION</th>
                        <th>PRICE</th>
                        <th class="text-right">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><img src="http://placehold.it/66x48" alt=""></td>
                        <td>
                            <h5>resumemarket cv design</h5>
                            <p>Single license<br>CVH9041</p>
                        </td>
                        <td>$8.00</td>
                        <td class="text-right">$8.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="medium-3 columns">
            <div class="coupon-box">
                <h6><strong>DISCOUNT CODE:</strong></h6>
                <p>Enter a gift certification or promotional code</p>
                <input type="text">
                <input type="submit" class="button radius" value="APPLY">
            </div>
        </div>
        <div class="medium-6 columns last">
            <table class="totals-table">
                <tbody>
                    <tr>
                        <td><strong>SUB TOTAL:</strong></td>
                        <td>$8.00</td>
                    </tr>
                    <tr>
                        <td><strong>DISCOUNT:</strong> 20% off <span style="color:#1c5043">Discount applied</span></td>
                        <td style="color:#cc0707;">-$2.00</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>TOTAL TO PAY:</td>
                        <td>$6.00</td>
                    </tr>
                </tfoot>
            </table>

            <div class="text-right">
                <button class="button clearfix" type="submit">
                    <span class="icon-wrapper"><img src="images/checkout2.png" alt=""></span>
                    <span class="name">CHECKOUT WITH PAYPAL</span>
                </button>
            </div>

        </div>
    </div>
</div>


<?php include('footer.php') ?>