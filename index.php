<?php include('header.php') ?>


<div id="slideshow" class="slideshow">
    <div>
        <div class="row">
            <div class="columns">
                <img src="images/slide1.png" alt="">
                <h1>Creative cv platform</h1>
                <h3>a new way to build your cv in minutes</h3>
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.<br /> 
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.<br />
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!</p>
            </div>
        </div>
    </div>
    <div>
        <img class="full-width" src="http://placehold.it/1000x450" alt="">
    </div>
</div>


<section class="why-us">
    <div class="row">
        <div class="columns text-center">
            <h2 class="section-title">WHY CHOOSE RESUMES MARKET?<br /> <span></span></h2>
        </div>
        <div class="medium-4 columns">
            <img src="images/quick.png" alt="">
            <h3>QUICK</h3>
            <p>Quick and easy way to transform your cv into creative design using our online cv tool. Which offers tons of features.</p>
        </div>
        <div class="medium-4 columns">
            <img src="images/flexible.png" alt="">
            <h3>FLEXIBLE</h3>
            <p>All of our cv designs are very fleixible and goes well with any industry like lawyers, accountants, graphic designer, software developer.</p>
        </div>
        <div class="medium-4 columns">
            <img src="images/safe.png" alt="">
            <h3>SAFE</h3>
            <p>We never share sare your personal 
information to any third party companies. When you use our website, we give gurantee that your data is safe with us! </p>
        </div>
    </div>
</section>


<section class="get-your-cv">
    <div class="row">
        <div class="columns text-center">
            <h2 class="section-title">GET YOUR CV IN MINUTES<br /> <span></span></h2>
        </div>
        <div class="medium-3 columns text-center">
            <img src="images/cv-tool.png" alt="">
        </div>
        <div class="medium-3 columns text-center">
            <img src="images/save-cv.png" alt="">
        </div>
        <div class="medium-3 columns text-center">
            <img src="images/make-payment.png" alt="">
        </div>
        <div class="medium-3 columns text-center">
            <img src="images/ready-to-download.png" alt="">
        </div>
    </div>
</section>


<section class="our-team">
    <div class="row">
        <div class="columns text-center">
            <h2 class="section-title">OUR TEAM<br /> <span></span></h2>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="our-team-content">
                <div class="row">
                    <div class="medium-4 columns">
                        <h3>PASSIONATE</h3>
                        <p>Quick and easy way to transform your cv into creative design using our online cv tool. Which offers tons of features.</p>
                    </div>
                    <div class="medium-4 columns">
                        <h3>CREATIVE</h3>
                        <p>Quick and easy way to transform your cv into creative design using our online cv tool. Which offers tons of features.</p>
                    </div>
                    <div class="medium-4 columns">
                        <h3>ENTHUSIASTIC</h3>
                        <p>Quick and easy way to transform your cv into creative design using our online cv tool. Which offers tons of features.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div id="our-team-carousel" class="our-team-carousel text-center">
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team1.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team1.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team3.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team1.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team1.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
                <div>
                    <div class="image-wrapper">
                        <a href="#"><img src="images/team1.png" alt=""></a>
                        <a href="#"><div class="shadow"></div></a>
                    </div>
                    <h3>JAMES JACKSON</h3>
                    <h6>Art Director</h6>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-designs">
    <div class="row">
        <div class="medium-6 columns text-right">
            <a href="#" class="button radius">CUSTOM DESIGNS</a>
        </div>
        <div class="medium-6 columns text-left">
            <a href="#" class="button radius active">STANDARD DESIGNS</a>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <ul class="medium-block-grid-3 designs-list">
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$8</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$199.99</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$8</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$8</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$8</span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <img src="images/design.png" alt="">
                            <div class="image-details">
                                <div class="actions">
                                    <a href="#"><i class="link"></i></a>
                                    <a href="#"><i class="delete"></i></a>
                                    <a href="#"><i class="heart"></i></a>
                                    <a href="#"><i class="zoom"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="design-details clearfix">
                            <h4>James Jackson CV</h4>
                            <span class="price">$8</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns text-center"><a href="#" class="button radius">VIEW ALL</a></div>
    </div>
</section>


<section class="testimonials">
    <div class="row">
        <div class="columns text-center">
            <h2 class="section-title">TESTIMONIALS<br /> <span></span></h2>
        </div>
    </div>
    <div class="row">
        <div class="medium-8 columns">
            <div class="testimonial testimonial1">
                <div class="bubble">
                    <p>As soon as you can, use real and relevant words. If your site or application requires data input, enter the real deal. as you can, use real and relevant words. If your site or application requires data input, enter the real deal. </p>
                    <span class="quotes"></span>
                    <span class="triangle"></span>
                </div>
                <h5>Michael Jordan</h5>
                <h6>NBSC interactive, United Kingom</h6>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-5 columns">
            <div class="testimonial testimonial2">
                <div class="bubble">
                    <p>As soon as you can, use real and relevant words. If your site or application requires data input, enter the real deal. as you can, use</p>
                    <span class="quotes"></span>
                    <span class="triangle"></span>
                </div>
                <h5>Michael Jordan</h5>
                <h6>NBSC interactive, United Kingom</h6>
            </div>
        </div>
        <div class="medium-7 columns">
            <div class="testimonial testimonial3">
                <div class="bubble">
                    <p>As soon as you can, use real and relevant words. If your site or application requires data input, enter the real deal. as you can, use real and relevant words. If your site or application requires data input, enter the real deal. </p>
                    <span class="quotes"></span>
                    <span class="triangle"></span>
                </div>
                <h5>Michael Jordan</h5>
                <h6>NBSC interactive, United Kingom</h6>
            </div>
        </div>
    </div>
</section>


<section class="partners">
    <div class="row">
        <div class="columns text-center">
            <h2 class="section-title">OUR PROUD PARTNERS<br /> <span></span></h2>
        </div>
    </div>
    <div class="row">
        <div class="columns text-center">
            <img src="images/partners.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="columns text-center">
            <a href="#" class="button radius">VIEW COMPLETE LIST</a>
        </div>
    </div>
</section>


<?php include('footer.php') ?>