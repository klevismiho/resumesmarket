<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal" style="margin-bottom:0;">
                    <h1>MY CVS</h1>
                </div>
                <div class="page-title-icon" style="padding-top:15px;"><img src="images/page-title-my-cvs.png" alt=""></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 columns">
            <div class="sort-by clearfix">
                <label>Sort by</label>
                <select>
                    <option>Please Select</option>
                    <option>Option 1</option>
                </select>
            </div>
        </div>
        <div class="medium-8 columns text-right">
            <span id="show-additional-filters" class="clearfix has-tip tip-top" data-tooltip aria-haspopup title="Additional filters">
                <i class="fa fa-filter"></i>
                <i class="fa fa-sort-down"></i>
            </span>
        </div>
    </div>
</div>


<div class="row collapse">
    <div class="columns">
        <div class="additional-filters">
            <h4 class="clearfix">ADDITIONAL FILTERS <span class="close"><i class="fa fa-close"></i></span></h4>
            <div class="row">
                <div class="medium-3 columns">
                    <label>Search Keyword</label>
                    <input type="text">
                </div>
                <div class="medium-3 columns">
                    <label>Category</label>
                    <select>
                        <option>Option 1</option>
                        <option>Option 2</option>
                    </select>
                </div>
                <div class="medium-3 columns">
                    <label>Check box</label>
                    <input type="checkbox">
                </div>
                <div class="medium-3 columns text-right">
                    <input type="submit" value="SEARCH">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="main-content my-cvs-content">

    <div class="row">
        <div class="medium-3 columns">
            <div class="my-cvs-tabs">
                <a href="#" class="button active radius">DEMO CV</a>
                <a href="#" class="button radius">PURCHASED CV</a>
            </div>
        </div>
        <div class="medium-9 columns">
            <h2>DEMO CV</h2>
            
            <div class="table-wrapper">
                <table class="my-cvs-table">
                    <tr>
                        <td><img src="http://placehold.it/66x50" alt=""></td>
                        <td>
                            <h3>CV STANDARDS DESIGN</h3>
                            <p>ITEM CODE: CV-23455ER<br>CV FOR: JOHN CARTER</p>
                        </td>
                        <td><br>
                            <p>ORDER ID: 0234234<br>DATE: 26 july 2017</p>
                        </td>
                        <td class="text-right">
                            <i class="message unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Message"></i>
                            <i class="money unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Title here"></i>
                            <i class="buy-now unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Buy now"></i>
                            <i class="edit unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Edit"></i>
                            <i class="download unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Download"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="http://placehold.it/66x50" alt=""></td>
                        <td>
                            <h3>CV STANDARDS DESIGN</h3>
                            <p>ITEM CODE: CV-23455ER<br>CV FOR: JOHN CARTER</p>
                        </td>
                        <td><br>
                            <p>ORDER ID: 0234234<br>DATE: 26 july 2017</p>
                        </td>
                        <td class="text-right">
                            <i class="message has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Message"></i>
                            <i class="money has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Title here"></i>
                            <i class="buy-now has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Buy now"></i>
                            <i class="edit has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Edit"></i>
                            <i class="download has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Download"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="http://placehold.it/66x50" alt=""></td>
                        <td>
                            <h3>CV STANDARDS DESIGN</h3>
                            <p>ITEM CODE: CV-23455ER<br>CV FOR: JOHN CARTER</p>
                        </td>
                        <td><br>
                            <p>ORDER ID: 0234234<br>DATE: 26 july 2017</p>
                        </td>
                        <td class="text-right">
                            <i class="message has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Message"></i>
                            <i class="money has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Title here"></i>
                            <i class="buy-now has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Buy now"></i>
                            <i class="edit has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Edit"></i>
                            <i class="download unavailable has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Download not Available in Demo CV"></i>
                        </td>
                    </tr>
                </table>
            </div>
            
            <br><br><br><br>
            <div class="pagination-centered">
              <ul class="pagination">
                <li class="arrow unavailable"><a href=""><</a></li>
                <li class="current"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li class="unavailable"><a href="">&hellip;</a></li>
                <li><a href="">12</a></li>
                <li><a href="">13</a></li>
                <li class="arrow"><a href="">></a></li>
              </ul>
            </div>

        </div>
    </div>

</div>


<?php include('footer.php') ?>