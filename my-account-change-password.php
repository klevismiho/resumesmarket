<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>CHANGE PASSWORD</h1>
                </div>
                <div class="page-title-icon" style="padding-top:13px;"><img src="images/page-title-change-password.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content">

    <div class="row">
        <div class="columns">
            <div class="alert-box alert radius">
              PLEASE TRY AGAIN
            </div>
            <br>
        </div>
    </div>
    
    <div class="row">
        <div class="columns medium-centered">
            <form id="change-password-form" class="change-password-form">
                <p>Enter your current password and enter your new password.
For security reason, you are not allowed to change account  email.
For more information please contact us. </p>
                <p>
                    <label>TITLE <em>*</em></label>
                    james.alison@gmail.com
                </p>
                <p>
                    <label>OLD PASSWORD <em>*</em></label>
                    <input type="password" name="old_password">
                </p>
                <p>
                    <label>NEW PASSWORD <em>*</em></label>
                    <input type="password" name="new_password" id="password">
                </p>
                <p>
                    <label>CONFIRM NEW PASSWORD <em>*</em></label>
                    <input type="password" name="new_password_confirm">
                </p>
                <div class="row collapse">
                    <div class="columns">
                        <button class="button cancel clearfix" type="submit">
                            <span class="icon-wrapper"><img src="images/cancel.png" alt=""></span>
                            <span class="name">CANCEL</span>
                        </button>
                        <button class="button save clearfix" type="submit">
                            <span class="icon-wrapper"><img src="images/save-changes.png" alt=""></span>
                            <span class="name">UPDATE NOW</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</div>


<?php include('footer.php') ?>