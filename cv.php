<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <title>Resumes Market</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600,600italic,400italic,300italic,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,400italic,300italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/cv.css">
    <script src="bower_components/modernizr/modernizr.js"></script>
    <script>
    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {

            isMobile = true;
        } 

    if( isMobile ) {
        document.write('<style>');
        document.write('.row {width:auto;max-width: 1028px; }');
        document.write('</style>');
    }
    </script>   
</head>
<body>

<header class="header">
    <div class="header-top">
        <div class="row">
            <div class="columns text-right">
                <ul class="user-navigation">
                    <li class="languages">
                        <a href="#"><i class="flag"></i> <span>English</span> <i class="fa fa-sort-down"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-power-off"></i> <span>Sign Out</span></a></li>
                    <li>
                        <a href="#"><span>Welcome James!</span> <i class="fa fa-sort-down"></i></a> 
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="row">
            <div class="medium-3 columns">
                <a href="#"><img src="images/logo.png" alt=""></a>
            </div>
            <div class="medium-9 columns text-right">
                <div class="header-socials">
                    <img src="images/header-socials.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <nav class="navigation-main">
                <ul>
                    <li>
                        <a href="#">HOME</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">OUR SERVICES</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">CUSTOM CV DESIGN</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">STANDARD CV DESIGN</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">HOW IT WORKS</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">BUILD YOUR CV</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title">
                    <h1>CV DESIGN</h1>
                    <h3>CV DESIGN FOR IT PROFESSIONAL</h3>
                </div>
                <div class="page-price">$8</div>
                <a class="purchase">PURCHASE<small>PURCHASE ONE MORE LICENCE</small></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-description">
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-7 columns">
            <ul class="cv-navigation">
                <li>
                    <a href="#">
                        <span>CV DETAILS</span>
                        <img src="images/cv-details.png" alt="">
                        <img class="image-active" src="images/cv-details-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>REVIEWS</span>
                        <img src="images/reviews.png" alt="">
                        <img class="image-active" src="images/reviews-active.png" alt="">
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <span>CV TOOL</span>
                        <img src="images/cv-tool2.png" alt="">
                        <img class="image-active" src="images/cv-tool2-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>HOW IT WORKS</span>
                        <img src="images/how-it-works.png" alt="">
                        <img class="image-active" src="images/how-it-works-active.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
        <div class="medium-5 columns">
            <nav class="cv-actions clearfix">
                <ul>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-share.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-email.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-list.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-pdf.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-paint.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-save.png" alt=""></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>


<div class="main-content">

    <div class="row">
        <div class="columns">
            <div class="alert-box warning">
                You haven't purchase the CV yet. Once you purchase the CV then you will have full control to download the CV in Print ready PDF format file and other feature  will be enbled as well.
            </div>
        </div>
    </div>

    <div class="cv-container">
        <div class="content cv-content">
            
            <div class="cv-header">
                <div class="avatar-container">
                    <div class="avatar">
                        <img src="./images/cv/user.png" alt="Full Name" />
                    </div>
                </div>
                <h1 class="name">Michael Anderson<span class="position">Graphic Designer</span></h1>
                <div class="contact-list-details">
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/mobile.svg" width="10px" height="21px">
                        </span>
                        0044(0)27555444722222222
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/email.svg" width="19px" height="12px">
                        </span>
                        sukhmeet.panesar@surgicalmatrix.com
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/website.svg" width="18px" height="18px">
                        </span>
                        sukhmeet.panesar@surgicalmatrix.com
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/landline.svg" width="19px" height="19px">
                        </span>
                        0044(0)27555444722222222
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/skype.svg" width="19px" height="18px">
                        </span>
                        micheal.ander
                    </div>
                </div>
                <div class="intro clearfix">
                    An award winning creative thinker and innovator with the ability to exceed client expectations.<br />
                    Leading the creative direction of work across global projects with the most demanding high profile clients. A natural team leader that can articulate in mixed media and<br />
                    take a wider view of the core creative. Acknowledged for an advanced skill set in both 3d and 2d design disciplines and an exceptional knowledge of production processes<br />
                    and project management.
                </div>
                <div class="intro-stats-details">
                    <ul class="intro-stats">
                        <li>
                            <div class="icon">
                                <img src="./images/cv/en_flag.svg" width="33" height="20">
                            </div>
                            <div class="label">Nationality</div>
                            British
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/date_of_birth.svg" width="19" height="27">
                            </div>
                            <div class="label">D.O.B</div>
                            23/07/1980
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/marital_status.svg" width="26" height="27">
                            </div>
                            <div class="label">Marital Status</div>
                            Single
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/driving_license.svg" width="26" height="27">
                            </div>
                            <div class="label">Driving Licence</div>
                            Full Clean
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/references.svg" width="27" height="25">
                            </div>
                            <div class="label">References</div>
                            Available on Request
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/relocation.svg" width="17" height="26">
                            </div>
                            <div class="label">Relocation</div>
                            Yes
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/freelance.svg" width="18" height="24">
                            </div>
                            <div class="label">Available for Freelance</div>
                            No
                        </li>
                    </ul>
                </div>
            </div>
            <div class="cv-body clearfix">
                <div class="details left">

                    <div class="side-seperator">
                        <div class="dots-holder">
                            <div class="dot top"></div>
                            <div class="dot bottom"></div>
                        </div>
                    </div>

                    <h3 class="section-title left-side">Work Experience <img src="./images/cv/torso.svg" class="torso left"></h3>
                    <div class="activity">
                        <h4 class="name">Microsoft, UK <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Software developer</strong> (July 2003 - May 2013)
                        </div>
                        <p>&bull; Advisor to Ambassador and high level officials on all policies concerning media</p>
                        <p>&bull; Developed the Company's marketing plan</p>
                        <p>&bull; Organising press trips & preparation of press kits.</p>
                    </div>
                    <div class="activity">
                        <h4 class="name">Freemedia Technologies <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Software developer</strong> (July 2003 - May 2013)
                        </div>
                        <p>&bull; Developed the Company's marketing plan</p>
                        <p>&bull; Organising press trips & preparation of press kits.</p>
                    </div>
                    <div class="activity">
                        <h4 class="name">Name of the Company <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Software developer</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                    <div class="activity">
                        <h4 class="name">Name of the Company <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Software developer</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                    <div class="activity">
                        <h4 class="name">Freemedia Technologies <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Software developer</strong> (July 2003 - May 2013)
                        </div>
                        <p>&bull; Developed the Company's marketing plan</p>
                        <p>&bull; Organising press trips & preparation of press kits.</p>
                    </div>
                    
                    <h3 class="section-title edu left-side">Education / Certification <img src="./images/cv/edu.svg" class="edu left"></h3>
                    <div class="activity">
                        <h4 class="name">London College of Communication <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>BA in Business Management with Marketing</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                    <div class="activity">
                        <h4 class="name">Cambridge University, UK <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Master in Multimedia Technology</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                    <div class="activity">
                        <h4 class="name">Brooks University <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Graphic Design Certification</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                    <div class="activity">
                        <h4 class="name">MicroSoft <img src="./images/cv/plus.svg" class="plus"></h4>
                        <div class="meta">
                            <strong>Graphic Design Certification</strong> (July 2003 - May 2013)
                        </div>
                    </div>
                </div>
                <div class="details right">
                    <h3 class="section-title right-side"><img src="./images/cv/torso.svg" class="torso right"> Professional Skills</h3>
                    <div class="rating-levels">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="basic">Basic</td>
                                    <td class="average">Average</td>
                                    <td class="expert">Expert</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="professional-skills-list">
                        <li>
                            <span class="name">&bull; Graphic Design</span>
                            <div class="rating">
                                <ul data-rating-value="5" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Web Design</span>
                            <div class="rating">
                                <ul data-rating-value="3" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div>4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Interface Design</span>
                            <div class="rating">
                                <ul data-rating-value="6" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div class="full">6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; SEO</span>
                            <div class="rating">
                                <ul data-rating-value="2" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div>3</div></li>
                                    <li><div>4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <ul class="professional-skills-list">
                        <li>
                            <span class="name">&bull; Photoshop</span>
                            <div class="rating">
                                <ul data-rating-value="7" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div class="full">6</div></li>
                                    <li><div class="full">7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Illustrator</span>
                            <div class="rating">
                                <ul data-rating-value="6" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div class="full">6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; InDesign</span>
                            <div class="rating">
                                <ul data-rating-value="3" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div>4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Dreamweaver</span>
                            <div class="rating">
                                <ul data-rating-value="4" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; CSS/HTML</span>
                            <div class="rating">
                                <ul data-rating-value="5" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Javascript</span>
                            <div class="rating">
                                <ul data-rating-value="2" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div>3</div></li>
                                    <li><div>4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <ul class="professional-skills-list">
                        <li>
                            <span class="name">&bull; Content Writing</span>
                            <div class="rating">
                                <ul data-rating-value="7" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div class="full">6</div></li>
                                    <li><div class="full">7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Illustration</span>
                            <div class="rating">
                                <ul data-rating-value="6" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div class="full">4</div></li>
                                    <li><div class="full">5</div></li>
                                    <li><div class="full">6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span class="name">&bull; Spots</span>
                            <div class="rating">
                                <ul data-rating-value="3" class="dots">
                                    <li><div class="full">1</div></li>
                                    <li><div class="full">2</div></li>
                                    <li><div class="full">3</div></li>
                                    <li><div>4</div></li>
                                    <li><div>5</div></li>
                                    <li><div>6</div></li>
                                    <li><div>7</div></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <h3 class="section-title personal-skills-title right-side"><img src="./images/cv/torso.svg" class="torso right"> Personal Skills</h3>
                    <div class="skills-circles-container">
                        <ul class="personal-skills-list">
                            <li>
                                <div class="detail">
                                    <div class="label">
                                        Punctuality
                                        <span class="value">90%</span>
                                    </div>
                                </div>
                                <input class="knob" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="90">
                            </li>
                            <li>
                                <div class="detail">
                                    <div class="label">
                                        Creativity
                                        <span class="value">45%</span>
                                    </div>
                                </div>
                                <input class="knob" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="45">
                            </li>
                            <li>
                                <div class="detail">
                                    <div class="label">
                                        Goal Oriented
                                        <span class="value">80%</span>
                                    </div>
                                </div>
                                <input class="knob" data-width="115" daa-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="80">
                            </li>
                        </ul>
                    </div>
                    <h3 class="section-title award right-side"><img src="./images/cv/awards.svg" class="awards"> Awards</h3>
                    <ul class="awards-list">
                        <li>- Won top graphic designer award in university in 2013</li>
                        <li>- Illustrator award in 2012</li>
                        <li>- New Business development in 2013</li>
                    </ul>
                </div>
            </div>
            <div class="cv-foot">
                <h3 class="section-title">Activities & Interests</h3>
                <ul class="footer-icons">
                    <li>
                        <img src="./images/cv/icons_spining.svg" class="icon">
                        <div class="label">Spinning</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_running.svg" class="icon">
                        <div class="label">Running</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_yoga.svg" class="icon">
                        <div class="label">Yoga</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_mountain.svg" class="icon">
                        <div class="label">Mountaing Climbing</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_tennis.svg" class="icon">
                        <div class="label">Tennis</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_swimming.svg" class="icon">
                        <div class="label">Swimming</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_cooking.svg" class="icon">
                        <div class="label">Cooking</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_modern_art.svg" class="icon">
                        <div class="label">Modern Art</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_painting.svg" class="icon">
                        <div class="label">Painting</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_music.svg" class="icon">
                        <div class="label">Music</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_theatre.svg" class="icon">
                        <div class="label">Theatre</div>
                    </li>
                    <li>
                        <img src="./images/cv/icons_movies.svg" class="icon">
                        <div class="label">Movies</div>
                    </li>
                </ul>
                <img src="./images/cv/cv-foot.svg" class="seperator">
                <p class="address">898 Uxbridge Rd, Kingston Upon Thames, KT1 2PP, United kingdom</p>
            </div>
        </div>
    </div><!--cv-container-->
</div>


<footer class="footer">
    <div class="footer-top">
        <div class="row">
            <div class="medium-4 columns">
                <a href="#"><img class="logo" src="images/logo-large.png" alt=""></a>
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
 If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
 deal with any of your request in very professional way!</p>
            </div>
            <div class="medium-8 columns">
                <div class="row">
                    <div class="medium-4 columns">
                        <h6>About ResumesMarket</h6>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">How it works</a></li>
                            <li><a href="#">Advertise with us</a></li>
                            <li><a href="#">Reviews</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                    <div class="medium-4 columns">
                        <h6>Other Information</h6>
                        <ul>
                            <li><a href="#">Terms and Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Media kit</a></li>
                            <li><a href="#">Resume examples</a></li>
                            <li><a href="#">Account for Recruiter</a></li>
                            <li><a href="#">Custom Designs</a></li>
                        </ul>
                    </div>
                    <div class="medium-4 columns">
                        <h6>Other Information</h6>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Resume Tips</a></li>
                            <li><a href="#">Partners</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="columns text-right">
                        <img class="footer-paypal" src="images/paypal2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="row">
            <div class="medium-7 columns text-left">
                <img src="images/4d-studios.png" alt="">
                <img src="images/logo-small.png" alt="">
                <span class="copyright">&copy; 2015 Resume Market - All Rights Reserved.<br />Powered by 4D Studios Edinburgh</span>
            </div>
            <div class="medium-5 columns text-right">
                <div class="socials text-right">
                    <a href="#"><img alt="" src="images/linkedin.png"></a>
                    <a href="#"><img alt="" src="images/youtube.png"></a>
                    <a href="#"><img alt="" src="images/behance.png"></a>
                    <a href="#"><img alt="" src="images/googleplus.png"></a>
                    <a href="#"><img alt="" src="images/pinterest2.png"></a>
                    <a href="#"><img alt="" src="images/twitter2.png"></a>
                    <a href="#"><img alt="" src="images/facebook2.png"></a>
                    <a href="#"><img alt="" src="images/instagram2.png"></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="bower_components/slick.js/slick/slick.min.js"></script>
<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/knob.js"></script>
<script type="text/javascript" src="js/leanModal.min.js"></script>
<script type="text/javascript" src="js/jquery.print-preview.js"></script>
<script type="text/javascript" src="js/jquery.livequery.js"></script>
<script type="text/javascript" src="js/cv.js"></script>
<script src="js/app.js"></script>
</body>
</html>