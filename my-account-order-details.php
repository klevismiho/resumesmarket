<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
                <li><a href="#">Order History</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>ORDER DETAILS</h1>
                </div>
                <div class="page-title-icon"><img src="images/page-title-order-details.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content basket-content">
    <div class="row">
        <div class="columns">
            <div class="order-details-boxes clearfix">
                <ul>
                    <li>
                        <h4>ORDER ID</h4>
                        <p>OD-123545</p>
                    </li>
                    <li>
                        <h4>ORDER DATE</h4>
                        <p>29 july 2015</p>
                    </li>
                    <li>
                        <h4>TOTAL PAID</h4>
                        <p>$6.00</p>
                    </li>
                    <li>
                        <h4>PAYMENT METHOD</h4>
                        <img src="images/paypal.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <table class="basket-table">
                <thead>
                    <tr>
                        <th colspan="2">NAME AND DESCRIPTION</th>
                        <th>PRICE</th>
                        <th class="text-right">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><img src="http://placehold.it/66x48" alt=""></td>
                        <td>
                            <h5>resumemarket cv design</h5>
                            <p>Single license<br>CVH9041</p>
                        </td>
                        <td>$8.00</td>
                        <td class="text-right">$8.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="medium-3 columns">
            
        </div>
        <div class="medium-6 columns last">
            <table class="totals-table">
                <tbody>
                    <tr>
                        <td><strong>SUB TOTAL:</strong></td>
                        <td>$8.00</td>
                    </tr>
                    <tr>
                        <td><strong>DISCOUNT:</strong> 20% off <span style="color:#1c5043">Discount applied</span></td>
                        <td style="color:#cc0707;">-$2.00</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>TOTAL TO PAY:</td>
                        <td>$6.00</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="medium-9 columns medium-centered">
            <div class="for-more-information text-center">
                <p>For more information contact customer services email at:<br><strong>support@resumesmarket.com</strong></p>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php') ?>