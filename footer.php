<footer class="footer">
	<div class="footer-top">
		<div class="row">
			<div class="medium-4 columns">
				<a href="#"><img class="logo" src="images/logo-large.png" alt=""></a>
				<p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
 If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
 deal with any of your request in very professional way!</p>
			</div>
			<div class="medium-8 columns">
				<div class="row">
					<div class="medium-4 columns">
						<h6>About ResumesMarket</h6>
						<ul>
							<li><a href="#">About us</a></li>
							<li><a href="#">Contact us</a></li>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Advertise with us</a></li>
							<li><a href="#">Reviews</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
					</div>
					<div class="medium-4 columns">
						<h6>Other Information</h6>
						<ul>
							<li><a href="#">Terms and Conditions</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Media kit</a></li>
							<li><a href="#">Resume examples</a></li>
							<li><a href="#">Account for Recruiter</a></li>
							<li><a href="#">Custom Designs</a></li>
						</ul>
					</div>
					<div class="medium-4 columns">
						<h6>Other Information</h6>
						<ul>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Resume Tips</a></li>
							<li><a href="#">Partners</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns text-right">
						<img class="footer-paypal" src="images/paypal2.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="row">
			<div class="medium-7 columns text-left">
				<img src="images/4d-studios.png" alt="">
				<img src="images/logo-small.png" alt="">
				<span class="copyright">&copy; 2015 Resume Market - All Rights Reserved.<br />Powered by 4D Studios Edinburgh</span>
			</div>
			<div class="medium-5 columns">
				<div class="socials text-right">
					<a href="#"><img src="images/linkedin.png" alt=""></a>
					<a href="#"><img src="images/youtube.png" alt=""></a>
					<a href="#"><img src="images/behance.png" alt=""></a>
					<a href="#"><img src="images/googleplus.png" alt=""></a>
					<a href="#"><img src="images/pinterest2.png" alt=""></a>
					<a href="#"><img src="images/twitter2.png" alt=""></a>
					<a href="#"><img src="images/facebook2.png" alt=""></a>
					<a href="#"><img src="images/instagram2.png" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="bower_components/slick.js/slick/slick.min.js"></script>
<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="js/app.js"></script>
</body>
</html>
