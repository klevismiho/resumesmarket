<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>FAVOURITES</h1>
                </div>
                <div class="page-title-icon"><img src="images/page-title-favorites.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content">

    <div class="row">
        <div class="columns">

            <div class="table-wrapper">
                <table class="favorites-table">
                    <tr>
                        <td><img src="http://placehold.it/66x50" alt=""></td>
                        <td>
                            <h3>CV STANDARDS DESIGN</h3>
                            <p>ITEM CODE: CV-23455ER<br>CV FOR: JOHN CARTER</p>
                        </td>
                        <td class="text-right">
                            <i class="remove-favorite has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Remove"></i>
                            <i class="link has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Title here"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="http://placehold.it/66x50" alt=""></td>
                        <td>
                            <h3>CV STANDARDS DESIGN</h3>
                            <p>ITEM CODE: CV-23455ER<br>CV FOR: JOHN CARTER</p>
                        </td>
                        <td class="text-right">
                            <i class="remove-favorite has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Remove"></i>
                            <i class="link has-tip tip-top radius" data-tooltip aria-haspopup="true" title="Title here"></i>
                        </td>
                    </tr>
                </table>
            </div>
            
            <br><br><br><br>
            <div class="pagination-centered">
              <ul class="pagination">
                <li class="arrow unavailable"><a href=""><</a></li>
                <li class="current"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li class="unavailable"><a href="">&hellip;</a></li>
                <li><a href="">12</a></li>
                <li><a href="">13</a></li>
                <li class="arrow"><a href="">></a></li>
              </ul>
            </div>

        </div>
    </div>

</div>


<?php include('footer.php') ?>