$(document).foundation();

$('.checkbox-wrapper').on('click', function() {

  $(this).toggleClass('checked');

  if ($(this).hasClass('checked')) {
    $('input[type="checkbox"]', this).prop('checked', true);
  } else {
    $('input[type="checkbox"]', this).prop('checked', false);
  }

})


if($('#slideshow').length) {
	$('#slideshow').slick({
		dots: true,
		arrows: false,
	})
}

if($('#our-team-carousel').length) {
	$('#our-team-carousel').slick({
		slidesToShow: 3,
		slidesToScroll: 1
	})
}

if($('#cv-slideshow').length) {
	$('#cv-slideshow').slick({
		
	})
}


if($('#how-it-works-video').length) {
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	$('.video-overlay').on('click', function() {
		$(this).hide();
		$('#how-it-works-video').get(0).play();
	});
	if(isiPad) {
		$('#how-it-works-video').removeAttr('controls');
	}
}


if($('.designs').length) {
	var slider = $('.designs .image-wrapper').on('init', function(slick) {
		$('.designs .image-wrapper').show();
	}).slick({
		dots: true,
		prevArrow: '<span class="arrow-wrapper left-arrow"><i class="fa fa-arrow-left"></i></span>',
		nextArrow: '<span class="arrow-wrapper right-arrow"><i class="fa fa-arrow-right"></i></span>',
	})

	$('.designs .design-item').hover(function() {

	}, function() {
		slider.slick('slickGoTo', 0);
	})
}



if($('#show-additional-filters').length) {
	$('#show-additional-filters').on('click', function() {
		$('.additional-filters').slideToggle();
		$('#show-additional-filters').toggleClass('active');
	})
	$('.additional-filters .close').on('click', function() {
		$('.additional-filters').slideUp();
		$('#show-additional-filters').removeClass('active');
	})
}


/* Contact */
if($('#contact-form').length) {
	$('#contact-form').validate({
		rules: {
			name: 'required',
			email: 'required',
			nature: 'required',
			message: 'required',
		}
	})
}

/* Register/Login */
if($('#register-form').length) {
	$('#register-form').validate({
		rules: {
			first_name: 'required',
			last_name: 'required',
			email: 'required',
			password: 'required',
		}
	})
}
if($('#login-form').length) {
	$('#login-form').validate({
		rules: {
			email: 'required',
			password: 'required',
		}
	})
}

/* My Details */
if($('#my-details-form').length) {
	$('#my-details-form').validate({
		rules: {
			title: 'required',
			first_name: 'required',
			last_name: 'required',
		}
	})
}

/* Change password */
if($('#change-password-form').length) {
	$('#change-password-form').validate({
		rules: {
			old_password: 'required',
			new_password: 'required',
			new_password_confirm: {
				equalTo: '#password',
			}
		}
	})
}