/* --------------------------------------------------------------------------- */
    /*    Knob Progressbar Plugin
    /* --------------------------------------------------------------------------- */

$(function($) {

    if (navigator.userAgent.indexOf('Safari') != -1 &&
        navigator.userAgent.indexOf('Chrome') == -1) {
        $("body").addClass("safari");
    }

    var countFooterIcons = $('.footer-icons li').length;
    if(countFooterIcons == 5) {
        $('.footer-icons').css({'width': '690px', 'margin': '15px auto'})
    } else if(countFooterIcons == 4) {
        $('.footer-icons').css({'width': '550px', 'margin': '15px auto'})
    } else if(countFooterIcons == 3) {
        $('.footer-icons').css({'width': '413px', 'margin': '15px auto'})
    } else if(countFooterIcons == 2) {
        $('.footer-icons').css({'width': '276px', 'margin': '15px auto'})
    }

    var countIntroStatsIcons = $('.intro-stats li').length;
    if(countIntroStatsIcons == 4) {
        $('.intro-stats').css({'width': '540px', 'margin': '0 auto'})
    } else if(countIntroStatsIcons == 3) {
        $('.intro-stats').css({'width': '404px', 'margin': '0 auto'})
    } else if(countIntroStatsIcons == 2) {
        $('.intro-stats').css({'width': '270px', 'margin': '0 auto'})
    }

    $(".knob").knob({
        change : function (value) {
            console.log("change : " + value);
            console.log($(this));
        },
        release : function (value) {
            //console.log(this.$.attr('value'));
            console.log("release : " + value);
        },
        cancel : function () {
            console.log("cancel : ", this);
        },
        draw : function () {

            // "tron" case
            if(this.$.data('skin') == 'tron') {

                var a = this.angle(this.cv)  // Angle
                    , sa = this.startAngle          // Previous start angle
                    , sat = this.startAngle         // Start angle
                    , ea                            // Previous end angle
                    , eat = sat + a                 // End angle
                    , r = 1;

                this.g.lineWidth = this.lineWidth;

                this.o.cursor
                    && (sat = eat - 0.3)
                    && (eat = eat + 0.3);

                if (this.o.displayPrevious) {
                    ea = this.startAngle + this.angle(this.v);
                    this.o.cursor
                        && (sa = ea - 0.3)
                        && (ea = ea + 0.3);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                    this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }
    });
    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#first-knob-slider-range').noUiSlider({
        start: [ 90 ],
        range: {
            'min': [  0 ],
            'max': [ 100 ]
        }
    });
    }
    $('#first-knob-slider-range').on({
        slide: function(){
            var currentSlideValue = $('#edit-first-knob-value').text().slice(0, -3);
            console.log(currentSlideValue);
            $('#edit-first-knob').val(currentSlideValue).trigger('change');
            $("#edit-first-knob-value").text(currentSlideValue+'%');
        }
    });
    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#first-knob-slider-range').Link('lower').to($('#edit-first-knob-value'));
    }
    $('#edit-first-knob-value').text('90%');
    $("#edit-first-knob").knob({
        change : function (value) {
            var currentValue = value-1;
            if(currentValue>=0) {
                $('#first-knob-slider-range').val(currentValue);
                $("#edit-first-knob-value").text(currentValue+'%');
            }
        }
    });

    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#second-knob-slider-range').noUiSlider({
        start: [ 45 ],
        range: {
            'min': [  0 ],
            'max': [ 100 ]
        }
    });
    }
    $('#second-knob-slider-range').on({
        slide: function(){
            var currentSlideValue = $('#edit-second-knob-value').text().slice(0, -3);
            console.log(currentSlideValue);
            $('#edit-second-knob').val(currentSlideValue).trigger('change');
            $("#edit-second-knob-value").text(currentSlideValue+'%');
        }
    });
    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#second-knob-slider-range').Link('lower').to($('#edit-second-knob-value'));
    }
    $('#edit-second-knob-value').text('45%');
    $("#edit-second-knob").knob({
        change : function (value) {
            var currentValue = value-1;
            if(currentValue>=0) {
                $('#second-knob-slider-range').val(currentValue);
                $("#edit-second-knob-value").text(currentValue+'%');
            }
        }
    });

    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#third-knob-slider-range').noUiSlider({
        start: [ 80 ],
        range: {
            'min': [  0 ],
            'max': [ 100 ]
        }
    });
    }
    $('#third-knob-slider-range').on({
        slide: function(){
            var currentSlideValue = $('#edit-third-knob-value').text().slice(0, -3);
            console.log(currentSlideValue);
            $('#edit-third-knob').val(currentSlideValue).trigger('change');
            $("#edit-third-knob-value").text(currentSlideValue+'%');
        }
    });
    if ( $.isFunction($.fn.noUiSlider) ) {
    $('#third-knob-slider-range').Link('lower').to($('#edit-third-knob-value'));
    }
    $('#edit-third-knob-value').text('80%');
    $("#edit-third-knob").knob({
        change : function (value) {
            var currentValue = value-1;
            if(currentValue>=0) {
                $('#third-knob-slider-range').val(currentValue);
                $("#edit-third-knob-value").text(currentValue+'%');
            }
        }
    });

    // Example of infinite knob, iPod click wheel
    var v, up=0,down=0,i=0
        ,$idir = $("div.idir")
        ,$ival = $("div.ival")
        ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
        ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
    $("input.infinite").knob(
                        {
                        min : 0
                        , max : 20
                        , stopper : false
                        , change : function () {
                                        if(v > this.cv){
                                            if(up){
                                                decr();
                                                up=0;
                                            }else{up=1;down=0;}
                                        } else {
                                            if(v < this.cv){
                                                if(down){
                                                    incr();
                                                    down=0;
                                                }else{down=1;up=0;}
                                            }
                                        }
                                        v = this.cv;
                                    }
                        });


    jQuery('.starbox').each(function() {
        var starbox = jQuery(this);
        starbox.starbox({
            average: starbox.attr('data-start-value'),
            changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
            ghosting: starbox.hasClass('ghosting'),
            autoUpdateAverage: starbox.hasClass('autoupdate'),
            buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
            stars: starbox.attr('data-star-count') || 5
        }).bind('starbox-value-changed', function(event, value) {
            if(starbox.hasClass('random')) {
                var val = Math.random();
                starbox.next().text('Random: '+val);
                return val;
            } else {
                starbox.next().text('Clicked: '+value);
            }
        }).bind('starbox-value-moved', function(event, value) {
            starbox.next().text('Moved to: '+value);
        });
    });

    $('.rating').each(function(){
        var dots  = $(this).find('.dots'),
            value = $(dots).attr('data-rating-value');
        $('li div', dots).slice(0, value).addClass('full');
    });

    $('.repeat-table-row .rating-system .dots li').live({
        mouseenter: function () {
            var dots             = $(this).parent(),
                currentRatingVal = $(this).find('div').html();
            $('li div', dots).slice(0, 7).css('background', '#fff');
            $('li div', dots).slice(0, currentRatingVal).css('background', '#353535');
        },
        mouseleave: function () {
            var dots             = $(this).parent(),
                currentRatingVal = $(this).find('div').html();
            $('li div', dots).slice(0, 7).attr('style', ' ');
            $('li div', dots).slice(0, currentRatingVal).attr('style', ' ');
        }
    });

    $('.repeat-table-row .rating-system .dots li').live('click', function(e){
        var dots             = $(this).parent(),
            currentRatingVal = $(this).find('div').html();
        $('li div', dots).slice(0, 7).removeClass('full checked');
        $('li div', dots).slice(0, currentRatingVal).addClass('full');
        $(this).find('div').addClass('checked');
        e.preventDefault();
    });

    $('.repeat-table-row .remove').live('click', function(e){
        var currentModalOptions = $(this).parent().parent().parent();
        //console.log(currentModalOptions);
        var totalRows = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length;
        var currentRowsNumberLeft = 15-totalRows+1;
        //console.log(totalRows);
        //console.log(currentRowsNumberLeft);
        if(currentRowsNumberLeft>=0){
            $(currentModalOptions).find('.count-left-personal-skills').text(currentRowsNumberLeft);
        }
        $(this).parent().remove();
        e.preventDefault();
    });

    $('.repeat-table-separator-row .remove').live('click', function(e){
        var currentModalOptions = $(this).parent().parent().parent();
        //console.log(currentModalOptions);
        var totalRows = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length;
        var currentRowsNumberLeft = 15-totalRows+1;
        //console.log(totalRows);
        //console.log(currentRowsNumberLeft);
        if(currentRowsNumberLeft>=0){
            $(currentModalOptions).find('.count-left-personal-skills').text(currentRowsNumberLeft);
        }
        $(this).parent().remove();
        e.preventDefault();
    });

    $('.addRow').on('click', function(e){
        var currentModalOptions = $(this).parent().parent();
        var countAllRows = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length;
        var currentRowsNumberLeft = 15-countAllRows-1;
        //console.log(currentRowsNumberLeft);
        if(currentRowsNumberLeft>=0){
            $(currentModalOptions).find('.count-left-personal-skills').text(currentRowsNumberLeft);
        }

        var currentRowsNumber = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length+1;
        //console.log(currentRowsNumber);
        if(currentRowsNumber <= 15) {
            $(currentModalOptions).find('.table-repeater-body').append("<div class='repeat-table-row clearfix'><div class='remove'></div><div class='field'><input class='edit-field' type='text' autocomplete='off' maxlength='35'></div><div class='limiter'>Max. 35 Characters</div><div class='rating-system'><ul class='dots duplicate'><li><div>1</div></li><li><div>2</div></li><li><div>3</div></li><li><div>4</div></li><li><div>5</div></li><li><div>6</div></li><li><div>7</div></li></ul></div></div>");
            $(currentModalOptions).find('.table-repeater-body .repeat-table-row:last').find('.edit-field').focus();

            if($(currentModalOptions).find('.table-repeater-body').outerHeight() >= 300) {
                $(currentModalOptions).find('.table-repeater-body').css('overflow-y', 'scroll').addClass('bigger');
            } else {
                $(currentModalOptions).find('.table-repeater-body').css('overflow-y', 'auto').removeClass('bigger');
            }
        } else {
            alert('Only 15 personal skills are allowed.');
        }
        e.preventDefault();
    });

    $('.addSeparator').on('click', function(e){
        var currentModalOptions = $(this).parent().parent();
        //console.log(currentModalOptions);
        var countAllRows = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length;
        var currentRowsNumberLeft = 15-countAllRows-1;
        //console.log(currentRowsNumberLeft);
        if(currentRowsNumberLeft>=0){
            $(currentModalOptions).find('.count-left-personal-skills').text(currentRowsNumberLeft);
        }
        var currentRowsNumber = $(currentModalOptions).find('.repeat-table-row').length+$(currentModalOptions).find('.repeat-table-separator-row').length+1;
        //console.log(currentRowsNumber);
        if(currentRowsNumber <= 15) {
            $(currentModalOptions).find('.table-repeater-body').append("<div class='repeat-table-separator-row clearfix'><div class='remove'></div><div class='title'>Separator space</div></div>");

            if($(currentModalOptions).find('.table-repeater-body').outerHeight() >= 300) {
                $(currentModalOptions).find('.table-repeater-body').css('overflow-y', 'scroll').addClass('bigger');
            } else {
                $(currentModalOptions).find('.table-repeater-body').css('overflow-y', 'auto').removeClass('bigger');
            }
        } else {
            alert('Only 15 personal skills are allowed.');
        }
        e.preventDefault();
    });


    $('.choose-activity li').live('click', function(e){
        var currentClickedActivity = $(this).text();
        var activityPlace = $('.add-activities li.free-place:first');

        if(activityPlace.length){
            var cases = 0;
            $('.add-activities li').each(function() {
                if($(this).text() == currentClickedActivity) {
                    cases++;
                }
            });
            if(cases>=1) {
                alert('This activity already exist on your list.');
            } else {

                var leftActivitiesCounter = $('.add-activities li.free-place').length-1;
                if(leftActivitiesCounter>=0){
                    $('#count-left-activities').text(leftActivitiesCounter);
                }
                $(activityPlace).removeClass('free-place').html($(this).html());
                $(activityPlace).find('.holder').append('<div class="remove-activity"></div>');

                var max = -1;
                $('.add-activities li').each(function() {
                    var h = $(this).outerHeight();
                    max = h > max ? h : max;
                });
                $('.add-activities li').each(function() {
                    $(this).css('min-height', max);
                });
            }
        } else {
            alert('You can\'t add more then 12 activities.');
        }

        e.preventDefault();
    });
    $('.add-activities li .holder .remove-activity').live('click', function(e){

        var leftActivitiesCounter = $('.add-activities li.free-place').length+1;
        if(leftActivitiesCounter>=0){
            $('#count-left-activities').text(leftActivitiesCounter);
        }

        var removedActivity   = $(this).parent().parent(),
            currentActivityID = $(removedActivity).index()+1;
        $(removedActivity).addClass('free-place').html('<div class="holder"></div><div class="label">#'+ currentActivityID +'</div>');
        e.preventDefault();
    });

    $('#uploadPhoto').on('click', function(e){
        $('#photoAvatar').trigger('click');
        e.preventDefault();
    });

    $('.cancel-modal').on('click', function(e){
        $("#lean_overlay").fadeOut(200);
        $('.modal').css({"display":"none"});
        e.preventDefault();
    });

    $("#photoAvatar").on("change", function(){
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
 
        if (/^image/.test( files[0].type)){
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
 
            reader.onloadend = function(){
                $(".upload-image-content").hide();
                $(".edit-image-content").show();
                $("#previewPhoto").attr("src", this.result);
            }
        }
    });

    $('#updateAvatar').on('click', function(e){
        $(".upload-image-content").show();
        $(".edit-image-content").hide();
        e.preventDefault();
    });

    $('#deleteAvatar').on('click', function(e){
        $('#previewPhoto').attr('src', './images/detault-avatar.jpg');
        $('.avatar img').attr('src', './images/detault-avatar.jpg');
        $("#lean_overlay").fadeOut(200);
        $('.modal').css({"display":"none"});
        e.preventDefault();
    });

    $('#saveAvatar').on('click', function(e){;
        $('.avatar img').attr('src', $('#previewPhoto').attr('src'));
        $("#lean_overlay").fadeOut(200);
        $('.modal').css({"display":"none"});
        e.preventDefault();
    });

    $('a.print-preview').printPreview();

    $("a[rel*=leanModal]").leanModal();

    $(".edit-part").leanModal();
    $('.remove-activity-item').on('click', function(e){
        e.stopPropagation();
        $(this).parent().hide();
        e.preventDefault();
    });

    function tog(v){
        return v?'addClass':'removeClass';
    } 
    $(document).on('input', '.edit-field', function(){
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function( e ){
        $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
    }).on('click', '.onX', function(){
        $(this).removeClass('x onX').val('').change();
    });

    if ($('.edit-introduction-content .objective-introduction').length > 0) {
        updateIntroCountdown();
        $('.edit-introduction-content .objective-introduction').change(updateIntroCountdown);
        $('.edit-introduction-content .objective-introduction').keyup(updateIntroCountdown);
    }

    if ($('.home-address-content .h-address').length > 0) {
        updateAddressCountdown();
        $('.home-address-content .h-address').change(updateAddressCountdown);
        $('.home-address-content .h-address').keyup(updateAddressCountdown);
    }

});

function updateIntroCountdown() {
    var remaining = 675 - jQuery('.edit-introduction-content .objective-introduction').val().length;
    jQuery('.edit-introduction-content .text-limit .counter').text(remaining);
}
function updateAddressCountdown() {
    var remaining = 140 - jQuery('.home-address-content .h-address').val().length;
    jQuery('.home-address-content .text-limit .counter').text(remaining);
}