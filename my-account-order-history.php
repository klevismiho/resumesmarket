<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal" style="margin:0;">
                    <h1>ORDER HISTORY</h1>
                </div>
                <div class="page-title-icon" style="padding-top:11px;"><img src="images/page-title-order-history.png" alt=""></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 columns">
            <div class="sort-by clearfix">
                <label>Sort by</label>
                <select>
                    <option>Please Select</option>
                    <option>Option 1</option>
                </select>
            </div>
        </div>
        <div class="medium-8 columns text-right">
            <span id="show-additional-filters" class="clearfix has-tip tip-top" data-tooltip aria-haspopup title="Additional filters">
                <i class="fa fa-filter"></i>
                <i class="fa fa-sort-down"></i>
            </span>
        </div>
    </div>
</div>


<div class="row collapse">
    <div class="columns">
        <div class="additional-filters">
            <h4 class="clearfix">ADDITIONAL FILTERS <span class="close"><i class="fa fa-close"></i></span></h4>
            <div class="row">
                <div class="medium-3 columns">
                    <label>Search Keyword</label>
                    <input type="text">
                </div>
                <div class="medium-3 columns">
                    <label>Category</label>
                    <select>
                        <option>Option 1</option>
                        <option>Option 2</option>
                    </select>
                </div>
                <div class="medium-3 columns">
                    <label>Check box</label>
                    <input type="checkbox">
                </div>
                <div class="medium-3 columns text-right">
                    <input type="submit" value="SEARCH">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="main-content" style="padding-top:25px;">

    <div class="row">
        <div class="columns">
            
            <div class="table-wrapper">
                <table class="order-history-table">
                    <thead>
                        <tr>
                            <th>ORDER ID</th>
                            <th>ORDER DATE</th>
                            <th colspan="2">TOTAL PAID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>OD-123456</strong></td>
                            <td>Thursday, 23 july 2015</td>
                            <td>$8.00</td>
                            <td><img src="images/plus.png" alt=""></td>
                        </tr>
                        <tr>
                            <td><strong>OD-123456</strong></td>
                            <td>Thursday, 23 july 2015</td>
                            <td>$8.00</td>
                            <td><img src="images/plus.png" alt=""></td>
                        </tr>
                        <tr>
                            <td><strong>OD-123456</strong></td>
                            <td>Thursday, 23 july 2015</td>
                            <td>$8.00</td>
                            <td><img src="images/plus.png" alt=""></td>
                        </tr>
                    </tbody>
                </table>  
            </div>      
    
        </div>
    </div>

</div>


<div class="pagination-centered">
  <ul class="pagination">
    <li class="arrow unavailable"><a href=""><</a></li>
    <li class="current"><a href="">1</a></li>
    <li><a href="">2</a></li>
    <li><a href="">3</a></li>
    <li><a href="">4</a></li>
    <li class="unavailable"><a href="">&hellip;</a></li>
    <li><a href="">12</a></li>
    <li><a href="">13</a></li>
    <li class="arrow"><a href="">></a></li>
  </ul>
</div>
<br><br>

<?php include('footer.php') ?>