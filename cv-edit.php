<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <title>Resumes Market</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600,600italic,400italic,300italic,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,400italic,300italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/jquery.nouislider.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/cv.css">
    <script src="bower_components/modernizr/modernizr.js"></script>
    <script>
    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {

            isMobile = true;
        } 

    if( isMobile ) {
        document.write('<style>');
        document.write('.row {width:auto;max-width: 1028px; }');
        document.write('</style>');
    }
    </script>  
</head>
<body>

<header class="header">
    <div class="header-top">
        <div class="row">
            <div class="columns text-right">
                <ul class="user-navigation">
                    <li class="languages">
                        <a href="#"><i class="flag"></i> <span>English</span> <i class="fa fa-sort-down"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-power-off"></i> <span>Sign Out</span></a></li>
                    <li>
                        <a href="#"><span>Welcome James!</span> <i class="fa fa-sort-down"></i></a> 
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="row">
            <div class="medium-3 columns">
                <a href="#"><img src="images/logo.png" alt=""></a>
            </div>
            <div class="medium-9 columns text-right">
                <div class="header-socials">
                    <img src="images/header-socials.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <nav class="navigation-main">
                <ul>
                    <li>
                        <a href="#">HOME</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">OUR SERVICES</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">CUSTOM CV DESIGN</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">STANDARD CV DESIGN</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">HOW IT WORKS</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                    <li>
                        <a href="#">BUILD YOUR CV</a>
                        <span class="triangle-wrapper"><i></i></span>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title">
                    <h1>CV DESIGN</h1>
                    <h3>CV DESIGN FOR IT PROFESSIONAL</h3>
                </div>
                <div class="page-price">$8</div>
                <a class="purchase">PURCHASE<small>PURCHASE ONE MORE LICENCE</small></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-description">
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-7 columns">
            <ul class="cv-navigation">
                <li>
                    <a href="#">
                        <span>CV DETAILS</span>
                        <img src="images/cv-details.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>REVIEWS</span>
                        <img src="images/reviews.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>CV TOOL</span>
                        <img src="images/cv-tool2.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>HOW IT WORKS</span>
                        <img src="images/how-it-works.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
        <div class="medium-5 columns">
            <nav class="cv-actions clearfix">
                <ul>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-share.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-email.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-list.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-pdf.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-paint.png" alt=""></a>
                    </li>
                    <li data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Tooltip here">
                        <a href="#"><img src="images/cv-save.png" alt=""></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>


<div class="main-content">

    <div class="row">
        <div class="columns">
            <div class="alert-box warning">
                You haven't purchase the CV yet. Once you purchase the CV then you will have full control to download the CV in Print ready PDF format file and other feature  will be enbled as well.
            </div>
        </div>
    </div>

    <div class="cv-container">
        <div class="content cv-content">
            
            <div class="cv-header">
                <a href="#upload-image" rel="leanModal" class="button add-new-btn upload-pic-btn">ADD picture</a>
                <div href="#upload-image" class="avatar-container edit-part">
                    <div class="avatar">
                        <img src="./images/cv/user.png" alt="Full Name" />
                    </div>
                    <a href="#upload-image" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="upload-image" class="modal">
                    <div class="head">
                        <h3 class="title">Upload picture</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="upload-image-content">
                            <input type="file" id="photoAvatar" class="hide">
                            <a id="uploadPhoto" href="#" class="button large upload-image">
                                <img src="./images/cv/photo.svg">
                                Browse Picture
                            </a>
                            <div class="image-tip">Maximum 2 MB</div>
                        </div>
                        <div class="edit-image-content hide">
                            <div class="preview-image-holder">
                                <img id="previewPhoto" src="">
                            </div>
                            <div class="image-options">
                                <a id="updateAvatar" href="#" class="button">Update</a>
                                <a id="deleteAvatar" href="#" class="button">Delete</a>
                                <a id="saveAvatar" href="#" class="button">Save</a>
                                <a id="cancelAvatar" href="#" class="button cancel-modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 href="#edit-name" class="name edit-part">
                    Michael Anderson
                    <span class="position">Graphic Designer</span>
                    <a href="#edit-name" rel="leanModal" class="open-edit-box"></a>
                </h1>
                <div id="edit-name" class="modal">
                    <div class="head">
                        <h3 class="title">Name and Job Type</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="edit-name-content">
                            <table class="edit-details">
                                <tbody>
                                <tr>
                                    <td><label for="first_name">First Name</label></td>
                                    <td><input id="first_name" class="edit-field" type="text" placeholder="Enter First Name" required autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td><label for="last_name">Last Name</label></td>
                                    <td><input id="last_name" class="edit-field" type="text" placeholder="Enter Last Name" required autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td><label for="job_type">Job Type</label></td>
                                    <td>
                                        <input id="job_type" class="edit-field" type="text" placeholder="Enter Job Type" autocomplete="off">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><div class="tip">For Example: Graphic Dsigner</div></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="right">
                                            <a href="#" class="button">Save</a>
                                            <a href="#" class="button cancel-modal">Cancel</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div href="#edit-other-information" class="contact-list-details edit-part">
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/mobile.svg" width="10px" height="21px">
                        </span>
                        0044(0)27555444722222222
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/email.svg" width="19px" height="12px">
                        </span>
                        sukhmeet.panesar@surgicalmatrix.com
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/website.svg" width="18px" height="18px">
                        </span>
                        sukhmeet.panesar@surgicalmatrix.com
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/landline.svg" width="19px" height="19px">
                        </span>
                        0044(0)27555444722222222
                    </div>
                    <div class="contact-by">
                        <span class="icon">
                            <img src="./images/cv/skype.svg" width="19px" height="18px">
                        </span>
                        micheal.ander
                    </div>
                    <a href="#edit-other-information" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="edit-other-information" class="modal">
                    <div class="head">
                        <h3 class="title">Other Information</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="edit-other-information-content">
                            <table class="edit-details">
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><div class="tip begin">Leave fields empty if you do not want to publish on your CV.</div></td>
                                </tr>
                                <tr>
                                    <td><label for="mobile_other">Mobile</label></td>
                                    <td class="icon"><img src="./images/cv/mobile.svg" width="10" height="21"></td>
                                    <td><input id="mobile_other" class="edit-field" type="text" autocomplete="off" value="0044(0)27555444722222222" maxlength="25"></td>
                                    <td class="counter">Max: 25</td>
                                </tr>
                                <tr>
                                    <td><label for="email_other">Email</label></td>
                                    <td class="icon"><img src="./images/cv/email.svg" width="19" height="12"></td>
                                    <td><input id="email_other" class="edit-field" type="text" autocomplete="off" value="sukhmeet.panesar@surgicalmatrix.com" maxlength="55"></td>
                                    <td class="counter">Max: 55</td>
                                </tr>
                                <tr>
                                    <td><label for="website_other">Website</label></td>
                                    <td class="icon"><img src="./images/cv/website.svg" width="18" height="18"></td>
                                    <td><input id="website_other" class="edit-field" type="text" autocomplete="off" value="sukhmeet.panesar@surgicalmatrix.com" maxlength="55"></td>
                                    <td class="counter">Max: 55</td>
                                </tr>
                                <tr>
                                    <td><label for="website_other">Landline</label></td>
                                    <td class="icon"><img src="./images/cv/landline.svg" width="19" height="19"></td>
                                    <td><input id="website_other" class="edit-field" type="text" autocomplete="off" value="0044(0)27555444722222222" maxlength="30"></td>
                                    <td class="counter">Max: 30</td>
                                </tr>
                                <tr>
                                    <td><label for="website_other">Skype</label></td>
                                    <td class="icon"><img src="./images/cv/skype.svg" width="19" height="18"></td>
                                    <td><input id="website_other" class="edit-field" type="text" autocomplete="off" value="micheal.ander" maxlength="25"></td>
                                    <td class="counter">Max: 25</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="right">
                                            <a href="#" class="button">Save</a>
                                            <a href="#" class="button cancel-modal">Cancel</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div href="#edit-introduction" class="intro-bio intro edit-part clearfix">
                    An award winning creative thinker and innovator with the ability to exceed client expectations.<br />
                    Leading the creative direction of work across global projects with the most demanding high profile clients. A natural team leader that can articulate in mixed media and<br />
                    take a wider view of the core creative. Acknowledged for an advanced skill set in both 3d and 2d design disciplines and an exceptional knowledge of production processes<br />
                    and project management.
                    <a href="#edit-introduction" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="edit-introduction" class="modal">
                    <div class="head">
                        <h3 class="title">Objective / Introduction</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="edit-introduction-content">
                            <textarea class="objective-introduction" maxlength="675">An award winning creative thinker and innovator with the ability to exceed client expectations. Leading the creative direction of work across global projects with the most demanding high profile clients. A natural team leader that can articulate in mixed media and take a wider view of the core creative. Acknowledged for an advanced skill set in both 3d and 2d design disciplines and an exceptional knowledge of production processes and project management.</textarea>
                            <div class="options clearfix">
                                <div class="text-limit">
                                    Max:
                                    <span class="counter">675</span>
                                </div>
                                <div class="buttons">
                                    <a href="#" class="button">Save</a>
                                    <a href="#" class="button cancel-modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div href="#edit-intro-stats" class="intro-stats-details edit-part">
                    <ul class="intro-stats">
                        <li>
                            <div class="icon">
                                <img src="./images/cv/en_flag.svg" width="33" height="20">
                            </div>
                            <div class="label">Nationality</div>
                            British
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/date_of_birth.svg" width="19" height="27">
                            </div>
                            <div class="label">D.O.B</div>
                            23/07/1980
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/marital_status.svg" width="26" height="27">
                            </div>
                            <div class="label">Marital Status</div>
                            Single
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/driving_license.svg" width="26" height="27">
                            </div>
                            <div class="label">Driving Licence</div>
                            Full Clean
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/references.svg" width="27" height="25">
                            </div>
                            <div class="label">References</div>
                            Available on Request
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/relocation.svg" width="17" height="26">
                            </div>
                            <div class="label">Relocation</div>
                            Yes
                        </li>
                        <li>
                            <div class="icon">
                                <img src="./images/cv/freelance.svg" width="18" height="24">
                            </div>
                            <div class="label">Available for Freelance</div>
                            No
                        </li>
                    </ul>
                    <a href="#edit-intro-stats" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="edit-intro-stats" class="modal">
                    <div class="head">
                        <h3 class="title">Other Information</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="intro-stats-content">
                            <table class="edit-details">
                                <tbody>
                                <tr>
                                    <td><label for="nationality_info">Nationality</label></td>
                                    <td class="icon"><img src="./images/cv/passport.svg" width="16" height="28" class="passport"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="nationality_info">
                                                <option></option>
                                                <option>State</option>
                                                <option selected="selected">British</option>
                                                <option>USA</option>
                                                <option>France</option>
                                                <option>Gernany</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td><label for="date_of_birth_info">Date of Birth</label></td>
                                    <td class="icon"><img src="./images/cv/date_of_birth.svg" width="15" height="28" class="date_of_birth"></td>
                                    <td>
                                        <input id="date_of_birth_info" class="edit-field" type="date" value="1980-07-23" autocomplete="off">
                                    </td>
                                    <td class="field-tip double">Select empty if you do not want to publish or enter year of birth or full date or birth</td>
                                </tr>
                                <tr>
                                    <td><label for="marital_status_info">Marital Status</label></td>
                                    <td class="icon"><img src="./images/cv/marital_status.svg" width="26" height="28" class="marital_status"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="marital_status_info">
                                                <option></option>
                                                <option>Married</option>
                                                <option selected="selected">Single</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td><label for="driving_license_info">Driving Licence</label></td>
                                    <td class="icon"><img src="./images/cv/driving_license.svg" width="20" height="28" class="driving_license"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="driving_license_info">
                                                <option></option>
                                                <option selected="selected">Full Clean</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td><label for="references_info">References</label></td>
                                    <td class="icon"><img src="./images/cv/references.svg" width="20" height="28" class="references"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="references_info">
                                                <option></option>
                                                <option selected="selected">Available on Request</option>
                                                <option>Reference 1</option>
                                                <option>Reference 2</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td><label for="relocation_info">Relocation</label></td>
                                    <td class="icon"><img src="./images/cv/relocation.svg" width="15" height="28" class="relocation"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="relocation_info">
                                                <option></option>
                                                <option selected="selected">Yes</option>
                                                <option>Relocation 1</option>
                                                <option>Relocation 2</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td><label for="freelance_info">Freelance</label></td>
                                    <td class="icon"><img src="./images/cv/freelance.svg" width="15" height="28" class="freelance"></td>
                                    <td>
                                        <div class="dropdown-container">
                                            <select id="freelance_info">
                                                <option></option>
                                                <option>Yes</option>
                                                <option selected="selected">No</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="field-tip">Select empty if you do not want to publish</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="left">
                                            <a href="#" class="button">Save</a>
                                            <a href="#" class="button cancel-modal">Cancel</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cv-body clearfix">
                <div class="details left">
                    <div class="side-seperator">
                        <div class="dots-holder">
                            <div class="dot top"></div>
                            <div class="dot bottom"></div>
                        </div>
                    </div>

                    <div class="cv-section-title-container">
                        <div href="#edit-work-experience" class="edit-work-experience-container edit-part clearfix">
                            <h3 class="section-title left-side">Work Experience <img src="./images/cv/torso.svg" class="torso left"></h3>
                            <a href="#edit-work-experience" rel="leanModal" class="button add-new-btn">ADD NEW</a>
                        </div>
                    </div>
                    <div id="edit-work-experience" class="modal">
                        <div class="head">
                            <h3 class="title">Work Experience</h3>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-work-experience-content">
                                <table class="edit-details">
                                    <tbody>
                                    <tr>
                                        <td><label for="company_name">Company name</label></td>
                                        <td><input id="company_name" class="edit-field" type="text" autocomplete="off" maxlength="60"></td>
                                        <td class="counter">Max: 60</td>
                                    </tr>
                                    <tr>
                                        <td><label for="job_type_work">Job Type</label></td>
                                        <td><input id="job_type_work" class="edit-field" type="text" autocomplete="off" maxlength="55"></td>
                                        <td class="counter">Max: 55</td>
                                    </tr>
                                    <tr>
                                        <td><label for="start_date_work">Start Date</label></td>
                                        <td>
                                            <div class="dropdown-container date-dropdown">
                                                <select id="start_date_work">
                                                    <option value="0" selected="1">Month</option><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                </select>
                                            </div>
                                            <div class="dropdown-container date-dropdown">
                                                <select>
                                                    <option value="0" selected="1">Year</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option><option value="1915">1915</option><option value="1914">1914</option><option value="1913">1913</option><option value="1912">1912</option><option value="1911">1911</option><option value="1910">1910</option><option value="1909">1909</option><option value="1908">1908</option><option value="1907">1907</option><option value="1906">1906</option><option value="1905">1905</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="end_date_work">End Date</label></td>
                                        <td>
                                            <div class="dropdown-container date-dropdown">
                                                <select id="end_date_work">
                                                    <option value="0" selected="1">Month</option><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                </select>
                                            </div>
                                            <div class="dropdown-container date-dropdown">
                                                <select>
                                                    <option value="0" selected="1">Year</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option><option value="1915">1915</option><option value="1914">1914</option><option value="1913">1913</option><option value="1912">1912</option><option value="1911">1911</option><option value="1910">1910</option><option value="1909">1909</option><option value="1908">1908</option><option value="1907">1907</option><option value="1906">1906</option><option value="1905">1905</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="role_line_1">Role line 1</label></td>
                                        <td><input id="role_line_1" class="edit-field" type="text" autocomplete="off" maxlength="30"></td>
                                        <td class="counter">Max: 85</td>
                                    </tr>
                                    <tr>
                                        <td><label for="role_line_2">Role line 2</label></td>
                                        <td><input id="role_line_2" class="edit-field" type="text" autocomplete="off" maxlength="30"></td>
                                        <td class="counter">Max: 85</td>
                                    </tr>
                                    <tr>
                                        <td><label for="role_line_3">Role line 3</label></td>
                                        <td><input id="role_line_3" class="edit-field" type="text" autocomplete="off" maxlength="30"></td>
                                        <td class="counter">Max: 85</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="left">
                                                <a href="#" class="button">Save</a>
                                                <a href="#" class="button cancel-modal">Cancel</a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="activity-content-holder cleafix">
                        <div href="#edit-work-experience" class="activity-container edit-part">
                            <div class="activity">
                                <h4 class="name">Microsoft, UK <img src="./images/cv/plus.svg" class="plus"></h4>
                                <div class="meta">
                                    <strong>Software developer</strong> (July 2003 - May 2013)
                                </div>
                                <p>&bull; Advisor to Ambassador and high level officials on all policies concerning media</p>
                                <p>&bull; Developed the Company's marketing plan</p>
                                <p>&bull; Organising press trips & preparation of press kits.</p>
                            </div>
                            <a href="#edit-work-experience" rel="leanModal" class="open-edit-box"></a>
                            <a href="#" class="remove-activity-item"></a>
                        </div>
                        <div class="activity">
                            <h4 class="name">Freemedia Technologies <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Software developer</strong> (July 2003 - May 2013)
                            </div>
                            <p>&bull; Developed the Company's marketing plan</p>
                            <p>&bull; Organising press trips & preparation of press kits.</p>
                        </div>
                        <div class="activity">
                            <h4 class="name">Name of the Company <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Software developer</strong> (July 2003 - May 2013)
                            </div>
                        </div>
                        <div class="activity">
                            <h4 class="name">Name of the Company <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Software developer</strong> (July 2003 - May 2013)
                            </div>
                        </div>
                        <div class="activity">
                            <h4 class="name">Freemedia Technologies <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Software developer</strong> (July 2003 - May 2013)
                            </div>
                            <p>&bull; Developed the Company's marketing plan</p>
                            <p>&bull; Organising press trips & preparation of press kits.</p>
                        </div>
                    </div>
                    <div class="cv-section-title-container">
                        <div href="#edit-education-certification" class="edit-edu-cert-experience-container edit-part clearfix">
                            <h3 class="section-title edu left-side">Education / Certification <img src="images/cv/edu.svg" class="edu left"></h3>
                            <a href="#edit-education-certification" rel="leanModal" class="button add-new-btn">ADD NEW</a>
                        </div>
                    </div>
                    <div id="edit-education-certification" class="modal">
                        <div class="head">
                            <h3 class="title">Education</h3>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-education-certification-content">
                                <table class="edit-details">
                                    <tbody>
                                    <tr>
                                        <td><label for="university_or_college">University or College</label></td>
                                        <td><input id="university_or_college" class="edit-field" type="text" autocomplete="off" maxlength="60"></td>
                                        <td class="counter">Max: 60</td>
                                    </tr>
                                    <tr>
                                        <td><label for="type_edu">Type</label></td>
                                        <td><input id="type_edu" class="edit-field" type="text" autocomplete="off" maxlength="55"></td>
                                        <td class="counter">Max: 55</td>
                                    </tr>
                                    <tr>
                                        <td><label for="start_date_edu">Start Date</label></td>
                                        <td>
                                            <div class="dropdown-container date-dropdown">
                                                <select id="start_date_edu">
                                                    <option value="0" selected="1">Month</option><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                </select>
                                            </div>
                                            <div class="dropdown-container date-dropdown">
                                                <select>
                                                    <option value="0" selected="1">Year</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option><option value="1915">1915</option><option value="1914">1914</option><option value="1913">1913</option><option value="1912">1912</option><option value="1911">1911</option><option value="1910">1910</option><option value="1909">1909</option><option value="1908">1908</option><option value="1907">1907</option><option value="1906">1906</option><option value="1905">1905</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="end_date_work">End Date</label></td>
                                        <td>
                                            <div class="dropdown-container date-dropdown">
                                                <select id="end_date_work">
                                                    <option value="0" selected="1">Month</option><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>
                                                </select>
                                            </div>
                                            <div class="dropdown-container date-dropdown">
                                                <select>
                                                    <option value="0" selected="1">Year</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option><option value="1915">1915</option><option value="1914">1914</option><option value="1913">1913</option><option value="1912">1912</option><option value="1911">1911</option><option value="1910">1910</option><option value="1909">1909</option><option value="1908">1908</option><option value="1907">1907</option><option value="1906">1906</option><option value="1905">1905</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="left">
                                                <a href="#" class="button">Save</a>
                                                <a href="#" class="button cancel-modal">Cancel</a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="activity-content-holder cleafix">
                        <div href="#edit-education-certification" class="activity-container edit-part">
                            <div class="activity">
                                <h4 class="name">London College of Communication <img src="./images/cv/plus.svg" class="plus"></h4>
                                <div class="meta">
                                    <strong>BA in Business Management with Marketing</strong> (July 2003 - May 2013)
                                </div>
                            </div>
                            <a href="#edit-education-certification" rel="leanModal" class="open-edit-box"></a>
                            <a href="#" class="remove-activity-item"></a>
                        </div>
                        <div class="activity">
                            <h4 class="name">Cambridge University, UK <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Master in Multimedia Technology</strong> (July 2003 - May 2013)
                            </div>
                        </div>
                        <div class="activity">
                            <h4 class="name">Brooks University <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Graphic Design Certification</strong> (July 2003 - May 2013)
                            </div>
                        </div>
                        <div class="activity">
                            <h4 class="name">MicroSoft <img src="./images/cv/plus.svg" class="plus"></h4>
                            <div class="meta">
                                <strong>Graphic Design Certification</strong> (July 2003 - May 2013)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="details right">
                    <div class="cv-section-title-container">
                        <div href="#new-edit-professional-skills" class="edit-professional-skills-container edit-part clearfix">
                            <h3 class="section-title right-side"><img src="./images/cv/torso.svg" class="torso right"> Professional Skills</h3>
                            <a href="#new-edit-professional-skills" rel="leanModal" class="button add-new-btn">ADD NEW</a>
                        </div>
                    </div>
                    <div id="new-edit-professional-skills" class="modal">
                        <div class="head">
                            <h3 class="title">Professional Skills</h3>
                            <a href="#" class="addSeparator">Separator</a>
                            <a href="#" class="addRow">Add Line <img src="./images/cv/add-line.svg" width="29" height="28"></a>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-professional-skills-content">
                                <div class="table-repeater-head clearfix">
                                    <div class="main-head">
                                        <div class="row-limiter">
                                            Max: 15 lines
                                        </div>
                                        <div class="row-counter">
                                            <span class="count-left-personal-skills">14</span> Lines left
                                        </div>
                                    </div>
                                    <div class="rating-head">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="basic">Basic</td>
                                                <td class="average">Average</td>
                                                <td class="expert">Expert</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="table-repeater-body clearfix" style="overflow-y: auto;">
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div>1</div></li>
                                                <li><div>2</div></li>
                                                <li><div>3</div></li>
                                                <li><div>4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="options clearfix">
                                    <div class="right">
                                        <a href="#" class="button">Save</a>
                                        <a href="#" class="button cancel-modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cleafix"></div>
                    <div class="rating-levels edit-rating-levels-show">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="basic">Basic</td>
                                    <td class="average">Average</td>
                                    <td class="expert">Expert</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div href="#edit-professional-skills" class="edit-other-section-container edit-part clearfix">
                        <ul class="professional-skills-list">
                            <li>
                                <span class="name">&bull; Graphic Design</span>
                                <div class="rating">
                                    <ul data-rating-value="5" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Web Design</span>
                                <div class="rating">
                                    <ul data-rating-value="3" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div>4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Interface Design</span>
                                <div class="rating">
                                    <ul data-rating-value="6" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div class="full">6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; SEO</span>
                                <div class="rating">
                                    <ul data-rating-value="2" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div>3</div></li>
                                        <li><div>4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <ul class="professional-skills-list">
                            <li>
                                <span class="name">&bull; Photoshop</span>
                                <div class="rating">
                                    <ul data-rating-value="7" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div class="full">6</div></li>
                                        <li><div class="full">7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Illustrator</span>
                                <div class="rating">
                                    <ul data-rating-value="6" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div class="full">6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; InDesign</span>
                                <div class="rating">
                                    <ul data-rating-value="3" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div>4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Dreamweaver</span>
                                <div class="rating">
                                    <ul data-rating-value="4" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; CSS/HTML</span>
                                <div class="rating">
                                    <ul data-rating-value="5" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Javascript</span>
                                <div class="rating">
                                    <ul data-rating-value="2" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div>3</div></li>
                                        <li><div>4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <ul class="professional-skills-list">
                            <li>
                                <span class="name">&bull; Content Writing</span>
                                <div class="rating">
                                    <ul data-rating-value="7" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div class="full">6</div></li>
                                        <li><div class="full">7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Illustration</span>
                                <div class="rating">
                                    <ul data-rating-value="6" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div class="full">4</div></li>
                                        <li><div class="full">5</div></li>
                                        <li><div class="full">6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <span class="name">&bull; Spots</span>
                                <div class="rating">
                                    <ul data-rating-value="3" class="dots">
                                        <li><div class="full">1</div></li>
                                        <li><div class="full">2</div></li>
                                        <li><div class="full">3</div></li>
                                        <li><div>4</div></li>
                                        <li><div>5</div></li>
                                        <li><div>6</div></li>
                                        <li><div>7</div></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <a href="#edit-professional-skills" rel="leanModal" class="open-edit-box"></a>
                    </div>
                    <div id="edit-professional-skills" class="modal">
                        <div class="head">
                            <h3 class="title">Professional Skills</h3>
                            <a href="#" class="addSeparator">Separator</a>
                            <a href="#" class="addRow">Add Line <img src="./images/cv/add-line.svg" width="29" height="28"></a>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-professional-skills-content">
                                <div class="table-repeater-head clearfix">
                                    <div class="main-head">
                                        <div class="row-limiter">
                                            Max: 15 lines
                                        </div>
                                        <div class="row-counter">
                                            <span class="count-left-personal-skills">1</span> Lines left
                                        </div>
                                    </div>
                                    <div class="rating-head">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="basic">Basic</td>
                                                    <td class="average">Average</td>
                                                    <td class="expert">Expert</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="table-repeater-body clearfix bigger" style="overflow-y: scroll;">
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Graphic Design" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Web Design" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div>4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Interface Design" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div class="full">6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="SEO" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div>3</div></li>
                                                <li><div>4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-separator-row clearfix"><div class="remove"></div><div class="title">Separator space</div></div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Photoshop" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div class="full">6</div></li>
                                                <li><div class="full">7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Illustrator" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div class="full">6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="InDesign" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div>4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Dreamweaver" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="CSS/HTML" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Javascript" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div>3</div></li>
                                                <li><div>4</div></li>
                                                <li><div>5</div></li>
                                                <li><div>6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-separator-row clearfix"><div class="remove"></div><div class="title">Separator space</div></div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Content Writing" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div class="full">6</div></li>
                                                <li><div class="full">7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="repeat-table-row clearfix">
                                        <div class="remove"></div>
                                        <div class="field">
                                            <input class="edit-field" type="text" value="Illustration" autocomplete="off" maxlength="35">
                                        </div>
                                        <div class="limiter">
                                            Max. 35 Characters
                                        </div>
                                        <div class="rating-system">
                                            <ul class="dots">
                                                <li><div class="full">1</div></li>
                                                <li><div class="full">2</div></li>
                                                <li><div class="full">3</div></li>
                                                <li><div class="full">4</div></li>
                                                <li><div class="full">5</div></li>
                                                <li><div class="full">6</div></li>
                                                <li><div>7</div></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="options clearfix">
                                    <div class="right">
                                        <a href="#" class="button">Save</a>
                                        <a href="#" class="button cancel-modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div href="#edit-personal-skills" class="edit-other-section-container edit-personal-skills-title-container edit-part clearfix">
                        <h3 class="section-title right-side"><img src="./images/cv/torso.svg" class="torso right"> Personal Skills</h3>
                        <div class="skills-circles-container">
                            <ul class="personal-skills-list">
                                <li>
                                    <div class="detail">
                                        <div class="label">
                                            Punctuality
                                            <span class="value">90%</span>
                                        </div>
                                    </div>
                                    <input class="knob" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="90">
                                </li>
                                <li>
                                    <div class="detail">
                                        <div class="label">
                                            Creativity
                                            <span class="value">45%</span>
                                        </div>
                                    </div>
                                    <input class="knob" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="45">
                                </li>
                                <li>
                                    <div class="detail">
                                        <div class="label">
                                            Goal Oriented
                                            <span class="value">80%</span>
                                        </div>
                                    </div>
                                    <input class="knob" data-width="115" daa-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" data-linecap="round" readonly value="80">
                                </li>
                            </ul>
                        </div>
                        <a href="#edit-personal-skills" rel="leanModal" class="open-edit-box"></a>
                    </div>
                    <div id="edit-personal-skills" class="modal">
                        <div class="head">
                            <h3 class="title">Personal Skills</h3>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-personal-skills-content">
                                <ul class="personal-skills-list">
                                    <li>
                                        <div class="detail">
                                            <div class="label">
                                                <span id="edit-first-knob-value" class="value">90%</span>
                                            </div>
                                        </div>
                                        <input id="edit-first-knob" type="text" data-readOnly="false" data-displayPrevious="true" data-angleOffset="0" data-displayPrevious="false" data-displayInput="false" data-linecap="round" data-cursor="false" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" value="90">
                                        <div id="first-knob-slider-range"></div>
                                        <div class="dropdown-container">
                                            <select>
                                                <option></option>
                                                <option selected="selected">Punctuality</option>
                                                <option>Creativity</option>
                                                <option>Goal Oriented</option>
                                            </select>
                                        </div>
                                        <div class="choose-tip">
                                            For example: Goal Oriented
                                        </div>
                                    </li>
                                    <li>
                                        <div class="detail">
                                            <div class="label">
                                                <span id="edit-second-knob-value" class="value">45%</span>
                                            </div>
                                        </div>
                                        <input id="edit-second-knob" type="text" data-readOnly="false" data-displayPrevious="true" data-angleOffset="0" data-displayPrevious="false" data-displayInput="false" data-linecap="round" data-cursor="false" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" value="45">
                                        <div id="second-knob-slider-range"></div>
                                        <div class="dropdown-container">
                                            <select>
                                                <option></option>
                                                <option>Punctuality</option>
                                                <option selected="selected">Creativity</option>
                                                <option>Goal Oriented</option>
                                            </select>
                                        </div>
                                        <div class="choose-tip">
                                            For example: Goal Oriented
                                        </div>
                                    </li>
                                    <li>
                                        <div class="detail">
                                            <div class="label">
                                                <span id="edit-third-knob-value" class="value">80%</span>
                                            </div>
                                        </div>
                                        <input id="edit-third-knob" type="text" data-readOnly="false" data-displayPrevious="true" data-angleOffset="0" data-displayPrevious="false" data-displayInput="false" data-linecap="round" data-cursor="false" data-width="115" data-height="115" data-fgColor="#353535" data-bgColor="#d2d4d8" data-thickness=".11" value="80">
                                        <div id="third-knob-slider-range"></div>
                                        <div class="dropdown-container">
                                            <select>
                                                <option></option>
                                                <option>Punctuality</option>
                                                <option>Creativity</option>
                                                <option selected="selected">Goal Oriented</option>
                                            </select>
                                        </div>
                                        <div class="choose-tip">
                                            For example: Goal Oriented
                                        </div>
                                    </li>
                                </ul>
                                <div class="options clearfix">
                                    <a href="#" class="button">Save</a>
                                    <a href="#" class="button cancel-modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div href="#edit-other-section" class="edit-other-section-container edit-awards-title-container edit-part clearfix">
                        <h3 class="section-title award right-side"><img src="./images/cv/awards.svg" class="awards"> Awards</h3>
                        <ul class="awards-list">
                            <li>- Won top graphic designer award in university in 2013</li>
                            <li>- Illustrator award in 2012</li>
                            <li>- New Business development in 2013</li>
                        </ul>
                        <a href="#edit-other-section" rel="leanModal" class="open-edit-box"></a>
                    </div>
                    <div id="edit-other-section" class="modal">
                        <div class="head">
                            <h3 class="title">Other</h3>
                            <a href="javascript:;" class="close-modal-x" title="Close"></a>
                        </div>
                        <div class="body">
                            <div class="edit-other-section-content">
                                <table class="edit-details">
                                    <tbody>
                                    <tr>
                                        <td><label for="select_icon_other_section">Select Icon</label></td>
                                        <td>
                                            <div class="dropdown-container small">
                                            <select id="select_icon_other_section">
                                                <option></option>
                                                <option>Running</option>
                                                <option>Spinning</option>
                                                <option>Swimming</option>
                                                <option>Yoga</option>
                                                <option>Mountaion Climbing</option>
                                                <option>Tennis</option>
                                            </select>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="heading_other_section">Heading</label></td>
                                        <td><input id="heading_other_section" class="edit-field" type="text" autocomplete="off" maxlength="40"></td>
                                        <td class="counter">Max: 40</td>
                                    </tr>
                                    <tr>
                                        <td><label for="line_1">Line 1</label></td>
                                        <td><input id="line_1" class="edit-field" type="text" autocomplete="off" maxlength="80"></td>
                                        <td class="counter">Max: 80</td>
                                    </tr>
                                    <tr>
                                        <td><label for="line_2">Line 2</label></td>
                                        <td><input id="line_2" class="edit-field" type="text" autocomplete="off" maxlength="80"></td>
                                        <td class="counter">Max: 80</td>
                                    </tr>
                                    <tr>
                                        <td><label for="line_3">Line 3</label></td>
                                        <td><input id="line_3" class="edit-field" type="text" autocomplete="off" maxlength="80"></td>
                                        <td class="counter">Max: 80</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="left">
                                                <a href="#" class="button">Save</a>
                                                <a href="#" class="button cancel-modal">Cancel</a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cv-foot">
                <div href="#edit-activities-interests" class="edit-activities-interests-container edit-part clearfix">
                    <h3 class="section-title">Activities & Interests</h3>
                    <ul class="footer-icons">
                        <li>
                            <img src="./images/cv/icons_spining.svg" class="icon">
                            <div class="label">Spinning</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_running.svg" class="icon">
                            <div class="label">Running</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_yoga.svg" class="icon">
                            <div class="label">Yoga</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_mountain.svg" class="icon">
                            <div class="label">Mountaing Climbing</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_tennis.svg" class="icon">
                            <div class="label">Tennis</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_swimming.svg" class="icon">
                            <div class="label">Swimming</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_cooking.svg" class="icon">
                            <div class="label">Cooking</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_modern_art.svg" class="icon">
                            <div class="label">Modern Art</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_painting.svg" class="icon">
                            <div class="label">Painting</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_music.svg" class="icon">
                            <div class="label">Music</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_theatre.svg" class="icon">
                            <div class="label">Theatre</div>
                        </li>
                        <li>
                            <img src="./images/cv/icons_movies.svg" class="icon">
                            <div class="label">Movies</div>
                        </li>
                    </ul>
                    <a href="#edit-activities-interests" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="edit-activities-interests" class="modal">
                    <div class="head">
                        <h3 class="title">Activities and Interests</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="edit-activities-interests-content">
                            <table class="activities-table">
                                <tbody>
                                <tr>
                                    <td class="available-activities">
                                        <div class="activities-head clearfix">
                                            <div class="head-tip">Click and add to the list</div>
                                            <div class="head-limiter">300 icons found</div>
                                        </div>
                                        <div class="activities-content">
                                            <ul class="available-circles choose-activity">
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_spining.svg" class="icon">
                                                    </div>
                                                    <div class="label">Spinning</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_running.svg" class="icon">
                                                    </div>
                                                    <div class="label">Running</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_yoga.svg" class="icon">
                                                    </div>
                                                    <div class="label">Yoga</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_mountain.svg" class="icon">
                                                    </div>
                                                    <div class="label">Mountaing Climbing</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_tennis.svg" class="icon">
                                                    </div>
                                                    <div class="label">Tennis</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_swimming.svg" class="icon">
                                                    </div>
                                                    <div class="label">Swimming</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_cooking.svg" class="icon">
                                                    </div>
                                                    <div class="label">Cooking</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_modern_art.svg" class="icon">
                                                    </div>
                                                    <div class="label">Modern Art</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_painting.svg" class="icon">
                                                    </div>
                                                    <div class="label">Painting</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_music.svg" class="icon">
                                                    </div>
                                                    <div class="label">Music</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_theatre.svg" class="icon">
                                                    </div>
                                                    <div class="label">Theatre</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_movies.svg" class="icon">
                                                    </div>
                                                    <div class="label">Movies</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_spining.svg" class="icon">
                                                    </div>
                                                    <div class="label">Spinning</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_running.svg" class="icon">
                                                    </div>
                                                    <div class="label">Running</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_yoga.svg" class="icon">
                                                    </div>
                                                    <div class="label">Yoga</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_mountain.svg" class="icon">
                                                    </div>
                                                    <div class="label">Mountaing Climbing</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_tennis.svg" class="icon">
                                                    </div>
                                                    <div class="label">Tennis</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_swimming.svg" class="icon">
                                                    </div>
                                                    <div class="label">Swimming</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_cooking.svg" class="icon">
                                                    </div>
                                                    <div class="label">Cooking</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_modern_art.svg" class="icon">
                                                    </div>
                                                    <div class="label">Modern Art</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_painting.svg" class="icon">
                                                    </div>
                                                    <div class="label">Painting</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_music.svg" class="icon">
                                                    </div>
                                                    <div class="label">Music</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_theatre.svg" class="icon">
                                                    </div>
                                                    <div class="label">Theatre</div>
                                                </li>
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_movies.svg" class="icon">
                                                    </div>
                                                    <div class="label">Movies</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="activities-seperator">
                                        <img src="./images/cv/move-arrow.svg" width="24" height="24">
                                    </td>
                                    <td class="added-activities">
                                        <div class="activities-head clearfix">
                                            <div class="head-tip">Maximum 12 Allowed</div>
                                            <div class="head-limiter"><span id="count-left-activities">11</span> left</div>
                                        </div>
                                        <div class="activities-content">
                                            <ul class="available-circles add-activities">
                                                <li>
                                                    <div class="holder">
                                                        <img src="./images/cv/icons_cooking.svg" class="icon">
                                                    <div class="remove-activity"></div>
                                                    </div>
                                                    <div class="label">Cooking</div>
                                                </li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#2</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#3</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#4</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#5</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#6</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#7</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#8</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#9</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#10</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#11</div></li>
                                                <li class="free-place"><div class="holder"></div><div class="label">#12</div></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="options clearfix">
                                <div class="right">
                                    <a href="#" class="button">Save</a>
                                    <a href="#" class="button cancel-modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="./images/cv/cv-foot.svg" class="seperator">
                <div href="#home-address" class="edit-address-container edit-part">
                    <p class="address">898 Uxbridge Rd, Kingston Upon Thames, KT1 2PP, United kingdom</p>
                    <a href="#home-address" rel="leanModal" class="open-edit-box"></a>
                </div>
                <div id="home-address" class="modal">
                    <div class="head">
                        <h3 class="title">Home address</h3>
                        <a href="javascript:;" class="close-modal-x" title="Close"></a>
                    </div>
                    <div class="body">
                        <div class="home-address-content">
                            <textarea class="h-address" maxlength="140"></textarea>
                            <div class="options clearfix">
                                <div class="text-limit">
                                    Max:
                                    <span class="counter">140</span>
                                </div>
                                <div class="buttons">
                                    <a href="#" class="button">Save</a>
                                    <a href="#" class="button cancel-modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--cv-container-->
</div>


<footer class="footer">
    <div class="footer-top">
        <div class="row">
            <div class="medium-4 columns">
                <a href="#"><img class="logo" src="images/logo-large.png" alt=""></a>
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
 If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
 deal with any of your request in very professional way!</p>
            </div>
            <div class="medium-8 columns">
                <div class="row">
                    <div class="medium-4 columns">
                        <h6>About ResumesMarket</h6>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">How it works</a></li>
                            <li><a href="#">Advertise with us</a></li>
                            <li><a href="#">Reviews</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                    <div class="medium-4 columns">
                        <h6>Other Information</h6>
                        <ul>
                            <li><a href="#">Terms and Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Media kit</a></li>
                            <li><a href="#">Resume examples</a></li>
                            <li><a href="#">Account for Recruiter</a></li>
                            <li><a href="#">Custom Designs</a></li>
                        </ul>
                    </div>
                    <div class="medium-4 columns">
                        <h6>Other Information</h6>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Resume Tips</a></li>
                            <li><a href="#">Partners</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="columns text-right">
                        <img class="footer-paypal" src="images/paypal2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="row">
            <div class="medium-7 columns text-left">
                <img src="images/4d-studios.png" alt="">
                <img src="images/logo-small.png" alt="">
                <span class="copyright">&copy; 2015 Resume Market - All Rights Reserved.<br />Powered by 4D Studios Edinburgh</span>
            </div>
            <div class="medium-5 columns text-right">
                <div class="socials text-right">
                    <a href="#"><img alt="" src="images/linkedin.png"></a>
                    <a href="#"><img alt="" src="images/youtube.png"></a>
                    <a href="#"><img alt="" src="images/behance.png"></a>
                    <a href="#"><img alt="" src="images/googleplus.png"></a>
                    <a href="#"><img alt="" src="images/pinterest2.png"></a>
                    <a href="#"><img alt="" src="images/twitter2.png"></a>
                    <a href="#"><img alt="" src="images/facebook2.png"></a>
                    <a href="#"><img alt="" src="images/instagram2.png"></a>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="js/jquery.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="bower_components/slick.js/slick/slick.min.js"></script>
<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/knob.js"></script>
<script type="text/javascript" src="js/leanModal.min.js"></script>
<script type="text/javascript" src="js/jquery.print-preview.js"></script>
<script type="text/javascript" src="js/jquery.livequery.js"></script>
<script type="text/javascript" src="js/cv.js"></script>
<script src="js/app.js"></script>
</body>
</html>