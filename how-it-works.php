<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title">
                    <h1>CV DESIGN</h1>
                    <h3>CV DESIGN FOR IT PROFESSIONAL</h3>
                </div>
                <div class="page-price">$8</div>
                <a class="purchase">PURCHASE<small>PURCHASE ONE MORE LICENCE</small></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-description">
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!<br><br></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <ul class="cv-navigation">
                <li>
                    <a href="#">
                        <span>CV DETAILS</span>
                        <img src="images/cv-details.png" alt="">
                        <img class="image-active" src="images/cv-details-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>REVIEWS</span>
                        <img src="images/reviews.png" alt="">
                        <img class="image-active" src="images/reviews-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>CV TOOL</span>
                        <img src="images/cv-tool2.png" alt="">
                        <img class="image-active" src="images/cv-tool2-active.png" alt="">
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <span>HOW IT WORKS</span>
                        <img src="images/how-it-works.png" alt="">
                        <img class="image-active" src="images/how-it-works-active.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>


<div class="main-content">
    <div class="row">
        <div class="columns">
            <div class="how-it-works-video">
                <div class="video-overlay"><button class="play"></button></div>
                <video controls id="how-it-works-video">
                  <source src="images/demo-video.mp4" type="video/mp4">
                  Your browser does not support HTML5 video.
                </video>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php') ?>