<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title">
                    <h1>CV DESIGN</h1>
                    <h3>CV DESIGN FOR IT PROFESSIONAL</h3>
                </div>
                <div class="page-price">$8</div>
                <a class="purchase">PURCHASE<small>PURCHASE ONE MORE LICENCE</small></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-description">
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!<br><br></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <ul class="cv-navigation">
                <li class="active">
                    <a href="#">
                        <span>CV DETAILS</span>
                        <img src="images/cv-details.png" alt="">
                        <img class="image-active" src="images/cv-details-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>REVIEWS</span>
                        <img src="images/reviews.png" alt="">
                        <img class="image-active" src="images/reviews-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>CV TOOL</span>
                        <img src="images/cv-tool2.png" alt="">
                        <img class="image-active" src="images/cv-tool2-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>HOW IT WORKS</span>
                        <img src="images/how-it-works.png" alt="">
                        <img class="image-active" src="images/how-it-works-active.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>


<section class="create-cv-in-minutes">
    <div class="row">
        <div class="columns">
            <div class="create-cv-in-minutes-content">
                <h1>CREATE CV</h1>
                <h3>IN MINUTES</h3>
                <h6>ONLY</h6>
                <h2>$8</h2>
            </div>
            <div class="lets-get-started">Let's get started</div>
        </div>
    </div>
</section>


<section class="likes-reviews-downloads">
    <div class="row">
        <div class="columns">
            <ul>
                <li>
                    <img src="images/downloads.png" alt="">
                    <h3>7,987</h3>
                    <h6>DOWNLOADS</h6>
                </li>
                <li>
                    <img src="images/reviews2.png" alt="">
                    <h3>5,987</h3>
                    <h6>REVIEWS</h6>
                </li>
                <li>
                    <img src="images/likes.png" alt="">
                    <h3>2,987</h3>
                    <h6>LIKES</h6>
                </li>
            </ul>
        </div>
    </div>
</section>


<section class="features">
    <div class="row">
        <div class="columns">
            <div class="feature-list">
                <h2>FEATURES</h2>
                <ul>
                    <li>
                        <h5>Free online Support</h5>
                        <p>Our 24 hours support always available to help you to resolve any issue</p>
                    </li>
                    <li>
                        <h5>Export into PDF</h5>
                        <p>Export into light weight PDF</p>
                    </li>
                    <li>
                        <h5>Multiple Colours</h5>
                        <p>Available in 10 different colours</p>
                    </li>
                    <li>
                        <h5>Edit anywhere</h5>
                        <p>Edit your CV anywhere in the world and no need any computer tools knowledge</p>
                    </li>
                    <li>
                        <h5>Suitable for any industry</h5>
                        <p>Suitable for any industry, law, doctor, IT Professional, Plumber</p>
                    </li>
                </ul>
            </div>
            <div class="socials-box">
                <ul>
                    <li><img src="images/facebook.png" alt=""></li>
                    <li><img src="images/pinterest.png" alt=""></li>
                    <li><img src="images/twitter.png" alt=""></li>
                    <li><img src="images/google.png" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="cv-slideshow">
    <div class="row">
        <div class="columns">
            <div id="cv-slideshow">
                <div>
                    <img src="images/cv-slideshow1.jpg" alt="">
                </div>
                <div>
                    <img src="images/cv-slideshow1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php') ?>