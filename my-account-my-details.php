<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>MY DETAILS</h1>
                </div>
                <div class="page-title-icon" style="padding-top:11px;"><img src="images/page-title-my-details.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content">

    <div class="row">
        <div class="columns">
            <div class="alert-box alert radius">
              ERROR ON FORM
            </div>
            <br>
        </div>
    </div>
    
    <div class="row">
        <div class="columns medium-centered">
            <form id="my-details-form" class="my-details-form">
                <p>
                    <label>TITLE <em>*</em></label>
                    <select name="title">
                        <option value="">Select</option>
                        <option value="1">Mr.</option>
                        <option value="2">Mrs.</option>
                    </select>
                </p>
                <p>
                    <label>FIRST NAME <em>*</em></label>
                    <input type="text" name="first_name">
                </p>
                <p>
                    <label>LAST NAME <em>*</em></label>
                    <input type="text" name="last_name">
                </p>
                <p>
                    <label>ADDRESS</label>
                    <input type="text" name="address">
                </p>
                <p>
                    <label>POSTCODE / ZIPCODE</label>
                    <input type="text" name="postcode">
                </p>
                <p>
                    <label>LOCATION / CITY / TOWN</label>
                    <input type="text" name="location">
                </p>
                <p>
                    <label>COUNTRY</label>
                    <select name="country">
                        <option value="">Select</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </p>
                <p>
                    <label>MOBILE</label>
                    <input type="text" name="mobile">
                </p>
                <p>
                    <label>LANDLINE</label>
                    <input type="text" name="landline">
                </p>
                <div class="row collapse">
                    <div class="columns">
                        <button class="button cancel clearfix" type="submit">
                            <span class="icon-wrapper"><img src="images/cancel.png" alt=""></span>
                            <span class="name">CANCEL</span>
                        </button>
                        <button class="button save clearfix" type="submit">
                            <span class="icon-wrapper"><img src="images/save-changes.png" alt=""></span>
                            <span class="name">SAVE CHANGES</span>
                        </button>
                    </div>
                </div>
                <p class="note text-right"><em>By clicking Save Changes you agree to our new T&C's</em></p>
            </form>
        </div>
    </div>
    
</div>


<?php include('footer.php') ?>