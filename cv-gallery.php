<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="columns">
            <img class="banner" src="images/banner.jpg" alt="">
        </div>
    </div>
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title extended">
                    <h1>CV DESIGNS</h1>
                    <h3>PURCHASE FORM</h3>
                </div>
                <div class="cv-found"><span class="nr">127</span><small>CV FOUND</small></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 columns">
            <div class="sort-by clearfix">
                <label>Sort by</label>
                <select>
                    <option>Please Select</option>
                    <option>Option 1</option>
                </select>
            </div>
        </div>
        <div class="medium-8 columns text-right">
            <span id="show-additional-filters" class="clearfix has-tip tip-top" data-tooltip aria-haspopup title="Additional filters">
                <i class="fa fa-filter"></i>
                <i class="fa fa-sort-down"></i>
            </span>
        </div>
    </div>
</div>


<div class="row collapse">
    <div class="columns">
        <div class="additional-filters">
            <h4 class="clearfix">ADDITIONAL FILTERS <span class="close"><i class="fa fa-close"></i></span></h4>
            <div class="row">
                <div class="medium-3 columns">
                    <label>Search Keyword</label>
                    <input type="text">
                </div>
                <div class="medium-3 columns">
                    <label>Category</label>
                    <select>
                        <option>Option 1</option>
                        <option>Option 2</option>
                    </select>
                </div>
                <div class="medium-3 columns">
                    <label>Check box</label>
                    <input type="checkbox">
                </div>
                <div class="medium-3 columns text-right">
                    <input type="submit" value="SEARCH">
                </div>
            </div>
        </div>
    </div>
</div>


<section class="designs">
    <div class="row collapse">
        <div class="columns">
            
            <ul class="medium-block-grid-3 designs-list">
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$88.00</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$199,99</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="slider-loading"></div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="design-item">
                        <div class="image-wrapper">
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                            <div><a href="#"><img src="images/design.png" alt=""></a></div>
                        </div>
                        <div class="slider-loading"></div>
                        <div class="design-details clearfix">
                            <h4><span>James Jackson CV</span><small>IT, Doctor, Law</small></h4>
                            <span class="price">$8</span>
                            <span class="favorite"><i class="fa fa-heart-o"></i><span>2,987</span></span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    
</section>


<div class="pagination-centered">
  <ul class="pagination">
    <li class="arrow unavailable"><a href=""><</a></li>
    <li class="current"><a href="">1</a></li>
    <li><a href="">2</a></li>
    <li><a href="">3</a></li>
    <li><a href="">4</a></li>
    <li class="unavailable"><a href="">&hellip;</a></li>
    <li><a href="">12</a></li>
    <li><a href="">13</a></li>
    <li class="arrow"><a href="">></a></li>
  </ul>
</div>


<section class="dream-job">
    <div class="row">
        <div class="columns text-center">
            <h2>Helping people globally to get their</h2>
            <h3>Dream Job!</h3>
        </div>
    </div>
</section>


<?php include('footer.php') ?>