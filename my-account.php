<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>MY ACCOUNT</h1>
                </div>
                <div class="page-title-icon"><img src="images/page-title-my-account.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content my-account-content">
    <div class="row">
        <div class="columns">
            <h2>WELCOME <span>JAMES JACKSON</span></h2>

            <nav class="my-account-menu">
                <ul>
                    <li class="my-details-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>My Details</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="order-history-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>Order History</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="my-cv-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>My CVs</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="my-favorites-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>My Favourites</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="messages-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>Messages</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="change-password-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>Change Password</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                    <li class="preferences-li">
                        <a href="#" class="clearfix">
                            <div class="icon-wrapper">
                                <i class="icon"></i>
                                <i class="icon-hover"></i>
                            </div>
                            <div class="my-account-menu-text">
                                <h3>Preferences</h3>
                                <p>Some text information</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>        
    </div>
</div>


<?php include('footer.php') ?>