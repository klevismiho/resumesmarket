<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">Register with us</a></li>
            </ul>
        </div>
    </div>
</div>


<div class="main-content register-content">
    <div class="row">
        <div class="columns text-center">
            <h1>Sign Up</h1>
            <p>Already have an account<br><a href="#">Log in</a></p>
            <div class="row sign-with-socials">
                <div class="medium-6 columns text-right">
                    <a href="#"><img src="images/facebook-register.png" alt=""></a>
                </div>
                <div class="medium-6 columns text-left">
                    <a href="#"><img src="images/linkedin-register.png" alt=""></a>
                </div>
            </div>
            <br>
            <br>
            <div class="or"><span>or</span></div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="alert-box alert radius">
              ERROR ON FORM
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="columns medium-centered">
            <form id="register-form" class="register-form">
                <p>
                    <label>FIRST NAME <em>*</em></label>
                    <input type="text" name="first_name">
                </p>
                <p>
                    <label>LAST NAME <em>*</em></label>
                    <input type="text" name="last_name">
                </p>
                <p>
                    <label>YOUR EMAIL ADDRESS <em>*</em></label>
                    <input type="email" name="email">
                </p>
                <p>
                    <label>PASSWORD <em>*</em></label>
                    <input type="password" name="password">
                </p>
                <p>
                    <label>LOCATION</label>
                    <input type="text" name="location">
                </p>
                <p>
                    <label>HOW DID YOU HEAR ABOUT?</label>
                    <select>
                        <option value="">Please select</option>
                        <option value="1">Option 1</option>
                    </select>
                </p>
                <button class="button clearfix" type="submit">
                    <span class="icon-wrapper"><img src="images/signup.png" alt=""></span>
                    <span class="name">SIGN UP</span>
                </button>
            </form>
        </div>
    </div>
    
</div>


<?php include('footer.php') ?>