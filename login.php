<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">Register with us</a></li>
            </ul>
        </div>
    </div>
</div>


<div class="main-content login-content">
    <div class="row">
        <div class="columns text-center">
            <h1>Sign Up</h1>
            <p>Already have an account<br><a href="#">Log in</a></p>
            <div class="row sign-with-socials">
                <div class="medium-6 columns text-right">
                    <a href="#"><img src="images/facebook-register.png" alt=""></a>
                </div>
                <div class="medium-6 columns text-left">
                    <a href="#"><img src="images/linkedin-register.png" alt=""></a>
                </div>
                <br>
                <br>
                <p><em>By clicking Log in, Facebook or Linkedin you agree to our T&C's</em></p>
            </div>
            <br>
            <br>
            <div class="or"><span>or</span></div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="alert-box alert radius">
              ERROR ON FORM
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="columns medium-centered">
            <form id="login-form" class="login-form">
                <p>
                    <label>EMAIL</label>
                    <input type="email" name="email">
                </p>
                <p>
                    <label>PASSWORD</label>
                    <input type="password" name="password">
                </p>
                <div class="row collapse">
                    <div class="medium-6 columns">
                        <span class="checkbox-wrapper">
                          <input type="checkbox">
                        </span>
                        <span class="remember-me">REMEMBER ME</span>
                    </div>
                    <div class="medium-6 columns text-right">
                        <a href="#" class="forgot-password">Forgot password?</a>
                    </div>
                </div>
                <button class="button clearfix" type="submit">
                    <span class="icon-wrapper"><img src="images/secure.png" alt=""></span>
                    <span class="name">SECURE LOGIN</span>
                </button>
            </form>
        </div>
    </div>
</div>


<?php include('footer.php') ?>