<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">CV Designs</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title">
                    <h1>CV DESIGN</h1>
                    <h3>CV DESIGN FOR IT PROFESSIONAL</h3>
                </div>
                <div class="page-price">$8</div>
                <a class="purchase">PURCHASE<small>PURCHASE ONE MORE LICENCE</small></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-description">
                <p>A new modern and quick way to transform your cv into creative design using our online cv tool.  
                Which offers tons of features like manage your education section, experience, technical skills, personal information, hobbies and interests.  
                Try out for free! but if you want to download in pdf format or forward directly to your employer then pay small amount and then you are on board. 
                If you have very complex CV, choose custom design from a gallery and one of our creative graphic  designer work on your CV and
                deal with any of your request in very professional way!<br><br></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <ul class="cv-navigation">
                <li>
                    <a href="#">
                        <span>CV DETAILS</span>
                        <img src="images/cv-details.png" alt="">
                        <img class="image-active" src="images/cv-details-active.png" alt="">
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <span>REVIEWS</span>
                        <img src="images/reviews.png" alt="">
                        <img class="image-active" src="images/reviews-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>CV TOOL</span>
                        <img src="images/cv-tool2.png" alt="">
                        <img class="image-active" src="images/cv-tool2-active.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>HOW IT WORKS</span>
                        <img src="images/how-it-works.png" alt="">
                        <img class="image-active" src="images/how-it-works-active.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>


<div class="main-content" style="padding-top:30px;">
    <div class="row">
        <div class="columns">

            <div class="reviews-wrapper">
                <div class="reviews-list">
                    <div class="sort-by clearfix">
                        <select>
                            <option>Please Select</option>
                            <option>Option 1</option>
                        </select>
                        <label>Sort by</label>
                    </div>
                    <div class="review-item">
                        <div class="review-header clearfix">
                            <h2>James Jackson</h2>
                            <div class="review-header-right">
                                <div class="review-stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star empty"></i>
                                </div>
                                <div class="review-date">
                                    Thursday, 22nd March 2014
                                </div>
                            </div>
                        </div>
                        <div class="review-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe a, dolorum eum omnis alias aspernatur veritatis voluptate ea vitae harum accusantium laboriosam earum blanditiis veniam repellendus officia dolorem, tenetur, quae.</p>
                        </div>
                    </div>
                    <div class="review-item">
                        <div class="review-header clearfix">
                            <h2>James Jackson</h2>
                            <div class="review-header-right">
                                <div class="review-stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star empty"></i>
                                </div>
                                <div class="review-date">
                                    Thursday, 22nd March 2014
                                </div>
                            </div>
                        </div>
                        <div class="review-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe a, dolorum eum omnis alias aspernatur veritatis voluptate ea vitae harum accusantium laboriosam earum blanditiis veniam repellendus officia dolorem, tenetur, quae.</p>
                        </div>
                    </div>
                </div><!--reviews-list-->
                
                <div class="pagination-centered">
                  <ul class="pagination">
                    <li class="arrow unavailable"><a href=""><</a></li>
                    <li class="current"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li class="unavailable"><a href="">&hellip;</a></li>
                    <li><a href="">12</a></li>
                    <li><a href="">13</a></li>
                    <li class="arrow"><a href="">></a></li>
                  </ul>
                </div>

            </div>

            <div class="total-rating">
                <h3>4.76</h3>
                <h4>AVERAGE BASED ON 7,4545 RATINGS</h4>
                <div class="total-stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star empty"></i>
                </div>
            </div>

        </div>

    </div>
</div>


<?php include('footer.php') ?>