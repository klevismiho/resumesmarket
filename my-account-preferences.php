<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>PREFERENCES</h1>
                </div>
                <div class="page-title-icon" style="padding-top:15px;"><img src="images/page-title-preferences.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content">
    <div class="row ">
        <div class="columns">
            <div class="table-wrapper">
                <table class="preferences-table">
                    <tr>
                        <td>SUBSCRIBE FOR NEWSLETTER?</td>
                        <td class="text-right">
                            <fieldset class="switch round" tabindex="0">
                              <input id="switch1" type="checkbox">
                              <label for="switch1"></label>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>CONTACT US QUERIES?</td>
                        <td class="text-right">
                            <fieldset class="switch round" tabindex="0">
                              <input id="switch2" type="checkbox">
                              <label for="switch2"></label>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</div>


<?php include('footer.php') ?>