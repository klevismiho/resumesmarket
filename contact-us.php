<?php include('header.php') ?>


<div class="page-header">
    <div class="row">
        <div class="medium-6 columns">
            <ul class="breadcrumbs">
                <li><a href="#">Home</a></li>
                <li><a href="#">Contact us</a></li>
            </ul>
        </div>
        <div class="medium-6 columns medium-text-right">
            <div class="cv-code">CV-00129993</div>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <div class="page-title-details">
                <div class="page-title page-title-normal">
                    <h1>CONTACT US</h1>
                </div>
                <div class="page-title-icon"><img src="images/page-title-phone.png" alt=""></div>
            </div>
        </div>
    </div>
</div>


<div class="main-content">
    <div class="row">
        <div class="columns">
            <div class="alert-box alert radius">
              ERROR ON FORM
            </div>
        </div>
    </div>
    <div class="row">
        <div class="medium-6 columns">
            <div class="contact-form-wrapper">
                <h2>DROP US A LINE</h2>
                <h6>WE WILL BACK TO YOU AS SOON AS POSSIBLE</h6>
                <form action="" id="contact-form">
                    <label>YOUR NAME <em>*</em></label>
                    <p><input type="text" name="name"></p>
                    <label>EMAIL <em>*</em></label>
                    <p><input type="email" name="email"></p>
                    <label>NATURE OF QUERY <em>*</em></label>
                    <p>
                        <select name="nature">
                            <option value="">Please select</option>
                            <option value="1">Option 1</option>
                        </select>
                    </p>
                    <label>MESSAGE <em>*</em></label>
                    <p><textarea name="message"></textarea></p>
                    <label>ENTER THE VERIFICATION CODE <em>*</em></label>
                    <input type="text">
                    <button class="button clearfix" type="submit">
                        <span class="icon-wrapper"><img src="images/send.png" alt=""></span>
                        <span class="name">SUBMIT</span>
                    </button>
                </form>
            </div>
        </div>
        <div class="medium-6 columns">
            <div class="contact-address">
                <br>
                <p><strong>Edinburgh</strong></p>
                <p>
                    21 Timber Bush<br>
                    Edinburgh EH6 6QH<br>
                    UK<br><br>

                    Phone: +44 131 555 4821<br>
                    Fax: +44 131 553 6821<br>
                    edinburgh@resumemarket.com
                </p>
                <br><br>
                <p><strong>Dubai</strong></p>
                <p>
                    21 Larnark Park<br>
                    Dubai, <br>
                    UAE<br><br>

                    Phone: +97<br>
                    Fax: +97<br>
                    dubai@resumemarket.com
                </p>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php') ?>